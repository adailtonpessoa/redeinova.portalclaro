var appRootPath = require('app-root-path');
var debug   	= appRootPath.require('./shared/debug')('synchronizer');
var Q		   	= require('q');

//Shared
var _handleError = appRootPath.require('./shared/handleError');

//Synchronizations
var _synchronizeClient  	= appRootPath.require('./synchronizations/synchronizeClient');
var _synchronizeUser 		= appRootPath.require('./synchronizations/synchronizeUser');
var _requestRouteSkipper	= appRootPath.require('./shared/requestRouteSkipper');

module.exports = function(request, response, next) {

	debug('starting...');

	var _license 		= request.license;
	var _myOrbeUser		= request.payload.usuario;

	var checkWhetherHasToSkip = function() {

		debug('checking whether has to skip...');

		if(_requestRouteSkipper.hasToSkip(request))
			return next();

		synchronizeClient()
			.then(synchronizeUser)
			.then(callNext)
			.catch(handleError)
			.done();
	};

	var handleError = function(error) {

		_handleError(response, error);
	};

	var synchronizeClient = function(license) {

		debug('synchronizing clients...');

		var applyClientToRequest = function(client) {

			debug('applying client to request...');

			request.client = client;
		};

		return _synchronizeClient(_license).then(applyClientToRequest);
	};

	var synchronizeUser = function() {

		debug('synchronizing user...');

		var clientId = request.client.id;

		var applyUserToRequest = function(user) {

			debug('applying user to request...');

			request.user = user;
		};

		return _synchronizeUser(_myOrbeUser, clientId).then(applyUserToRequest);
	};

	var callNext = function() {

		debug('calling next...');
		next();
	};

	checkWhetherHasToSkip();
};