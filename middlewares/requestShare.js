/**
 * Created by davisabino on 26/02/16.
 */
var appRootPath             = require('app-root-path');
var debug 		            = appRootPath.require('./shared/debug')('requestShare');
var q 			            = require('q');

//Shared
var _handleError 		    = appRootPath.require('./shared/handleError');
var _requestRouteSkipper    = appRootPath.require('./shared/requestRouteSkipper');

var UserShare               = appRootPath.require('./models/userShare');
var SimpleCache             = appRootPath.require('./shared/SimpleCache');
var config                  = appRootPath.require('./shared/config');
var utils                   = appRootPath.require('./shared/utils');

module.exports = function(request, response, next) {

    debug('starting...');
    var route = '';
    var parts = request.url.split('/');
    if(parts.length > 2)
        route = parts[2];
    else
        route = parts[1];

    debug('URL: %j', request.url);

    debug('Rota: %j', route);

    var callNext = function() {
        debug('calling next...');
        next();
    };

    function checkShare(){
        debug('Checando quota de uso para cliente');
        var share = SimpleCache.getFromCache('share_' + request.license.usuarioProprietario.id + "_" + route);
        if(!share){
            return getShareFromDB()
                .then(checkExists)
                .then(addToCache);
        }

        return q(share);
    }

    function getShareFromDB(){
        debug('Obtendo do database');
        var date = new Date();
        date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        var deferred = q.defer();

        debug("Finding Share for: %j", {
            idProprietario : request.license.usuarioProprietario.id,
            vigencia : date,
            recurso : route
        });

        UserShare.findOne({
            idProprietario : request.license.usuarioProprietario.id,
            vigencia : date,
            recurso : route
        }, function(err, share){


            if(!share)
                return deferred.reject({ status : 403, message : 'Cliente não possui acesso ao recurso solicitado'});
            debug('found Share: %j', share);
            return deferred.resolve(share);
        });

        return deferred.promise;
    }

    function checkExists(share){
        if(!share)
            return q.reject({ status : 403, message : 'Cliente não possui acesso ao recurso solicitado'});

        return q(share);

    }

    function addToCache(share){
        debug('Adicionando quota ao cache');
        SimpleCache.addToCache('share_' + request.license.usuarioProprietario.id + "_" + route,
        share, sincronize);
        return q(share);
    }


    function checkRequestLimit(share){
        share.quantidadeRequisicoes++;
        debug("Quota de requisições: %j", share);
        if(!share.ativo || share.quantidadeRequisicoes > share.limite.quantidadeRequisicoes){
            share.ativo = false;
            if(!share.foiExcedido){
                share.dataEmQueExcedeu = new Date();
                share.foiExcedido = true;
            }
            debug('Cliente execedeu quota de requisições mensal');
            return q.reject({ status : 403, message : 'Cliente execedeu quota de requisições mensal'});
        }

        return q();
    }

    function sincronize(share){
        debug('Saving share to database');
        return q(share.save());
    }

    checkShare()
    .then(checkRequestLimit)
    .then(callNext)
    .catch(utils.handleError(response))
    .done();
};