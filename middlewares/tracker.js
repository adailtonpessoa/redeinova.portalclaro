var appRootPath = require('app-root-path');
var rawBody 	= require('raw-body');
var debug 		= appRootPath.require('./shared/debug')('tracker');
var config 		= appRootPath.require('./shared/config');
var ipFinder 	= appRootPath.require('./shared/ipFinder');

var currentIp = ipFinder.find()[0];

var Trail = appRootPath.require('./models/trail');

module.exports = function tracker(request, response, next) {

	var end  	= response.end;
	var startAt = process.hrtime();
	var contentLength = parseInt((request.headers['content-length'] || 0) + ''); 
	var reqLength = 0;
	var hasreq =  request.url.indexOf('?');
	if(hasreq != -1){
		 reqLength = request.url.length - hasreq -1;
	}
	reqLength += contentLength;

	response.on('finish', () => {

		debug('ending...');

		var diff 		 = process.hrtime(startAt);
		var responseTime = Math.round(diff[0] * 1e3 + diff[1] * 1e-6);

		var idUsuarioProprietario = request.license && request.license.usuarioProprietario.id ?
							request.license.usuarioProprietario.id : '?';

		var idUsuarioOperador = request.payload && request.payload.usuario.id ?
							request.payload.usuario.id : '?';
		
		var trail = new Trail({
			idUsuarioProprietario: idUsuarioProprietario,
			idUsuarioOperador: idUsuarioOperador,
			url: request.url,
			metodoHttp: request.method,
			statusDaResposta: response.statusCode,
			mensagemDeErro: response.errorMessage,
			tempoDeResposta: responseTime,
			ip: request.headers['x-forwarded-for'] || request.ip,
			moduloId: config.moduleId,
			tamanhoDaRequisicao : reqLength,
			tamanhoDaResposta : response._contentLength,
			ipAPI: currentIp.address
	 	});

		debug('trail: %j', trail);
		debug('currentIp: %s', currentIp.address);

		trail.save();
	});

	next();
};