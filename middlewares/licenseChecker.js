var appRootPath = require('app-root-path');
var debug 		= appRootPath.require('./shared/debug')('licenseChecker');
var Q 			= require('q');
var appRootPath = require('app-root-path');

//Shared
var _handleError 			= appRootPath.require('./shared/handleError');
var _myOrbeDS 				= appRootPath.require('./data-sources/myOrbeDS');
var _requestRouteSkipper	= appRootPath.require('./shared/requestRouteSkipper');
var config                  = appRootPath.require('./shared/config');



module.exports = function(request, response, next) {

	debug('starting...');

	function checkWhetherItHasToSkip () {

		debug('checking whether it has to skip...');

		if(_requestRouteSkipper.hasToSkip(request))
			return next();

		checkPayload()
			.then(retrieveLicensesFromMyOrbe)
			.then(checkResult)
			.then(findForLicenses)
			.then(applyDataOfLicenseToRequest)
			.then(callNext)
			.catch(handleError)
			.done();
	}

	function handleError(error) {

		return  _handleError(response, error);
	}

	function checkPayload() {

		debug('checking userOrbeId...');

		var user = request.payload.usuario;

		if(!user) {

			debug('payload do not has user!');
			return Q.reject({ status: 401, message: 'Não autorizado', logObs: 'O payload não possui usuário' });
		}

		return Q();
	}

	function retrieveLicensesFromMyOrbe() {

		debug('retrieving licenses from my orbe...');

		debug(request.payload);
		var userOrbeId 	= request.payload.usuario.id;
		var signature 	= request.signature;

		return _myOrbeDS.getAllLicensesForMyOrbeUser(signature);
	}

	function checkResult(result) {

		debug('checking result: %j', result);

		if(typeof result == 'string') {

			if(result == 'Erro interno na API')
				return Q.reject({ status: 500, message: 'Erro interno na API de autenticação', logObs: 'Erro interno no MyOrbe' });

			return Q.reject({ status: 401, message: result, logObs: 'Erro do MyOrbe' });
		}

		return result;
	}

	function findForLicenses(licenses) {

		debug('finding for licenses...');
		console.log(licenses);

		var validModuleIds 	= [config.moduleId];
		var firstLicense 	= licenses.find(license => {

			return validModuleIds.some(id => id == license.modulo.id);
		});
		
		if(!firstLicense)
			return Q.reject({ status: 401, message: 'O usuário não possui licença para o módulo ' + config.moduleId });

		return Q(firstLicense);
	}

	function applyDataOfLicenseToRequest(license) {

		debug('applying data of license to request...');
		debug('license: %j', license);

		request.license = license;
	}

	function callNext() {

		debug('calling next...');
		next();
	}

	checkWhetherItHasToSkip();
};