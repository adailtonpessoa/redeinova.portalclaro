var appRootPath = require('app-root-path');
var debug 		= appRootPath.require('./shared/debug')('signaturePayloadExtractor');
var Q 			= require('q');


//Shared
var _handleError 		 = appRootPath.require('./shared/handleError');
var _requestRouteSkipper = appRootPath.require('./shared/requestRouteSkipper');

module.exports = function(request, response, next) {

	debug('starting...');

	var _authorizationHeader = request.headers['authorization'];

	var checkWhetherHasToSkip = function() {

		debug('checking whether has to skip...');

		if(_requestRouteSkipper.hasToSkip(request))
			return next();

		checkAuthorizationHeader()
			.then(extractPayload)
			.then(callNext)
			.catch(handleError)
			.done();
	};

	var handleError = function(error) {

		_handleError(response, error);
	};

	var checkAuthorizationHeader = function() {

		debug('checking authorization header...');

		if(!_authorizationHeader)
			return Q.reject({ status: 401, message: 'No authorization header in request' });

		var signature = _authorizationHeader.split(' ')[1];

		if(!signature)
			return Q.reject({ status: 401, message: 'Bad format for signature' });

		return Q(signature);
	};

	var extractPayload = function(signature) {

		debug('extracting payload...');

		var signatureParts 	= signature.split('.');
		var buffer 			= new Buffer(signatureParts[1], 'base64');
		var payloadString 	= buffer.toString();

		request.payload 	= JSON.parse(payloadString);
		request.signature 	= signature;
	};

	var callNext = function() {

		debug('calling next...');
		next();
	};

	checkWhetherHasToSkip();
};