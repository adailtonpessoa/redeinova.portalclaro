var appRootPath = require('app-root-path');
var debug     	= appRootPath.require('./shared/debug')('mongoDS');
var mongoose  	= require('mongoose');
var config 		= appRootPath.require('./shared/config');

debug('loading mongo');

exports.connect = function() {
	debug('connecting to mongo');
	mongoose.connect(config.mongoURL, function(error) {

	    if (error)
	        return console.error('Error trying to connect to MongoDB: %s', error);

	    console.log('Connected to MongoDB...');
	});
};