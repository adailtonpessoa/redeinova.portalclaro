var appRootPath 	= require('app-root-path');
var config 			= appRootPath.require('./shared/config');
var baseSqlServerDS	= appRootPath.require('./data-sources/baseSqlServerDS');

function sendCommand(command, parameters) {

	return baseSqlServerDS.sendCommand(config.sgsServer, command, parameters);
}

function callProcedure(procedureName, parameters) {

	return baseSqlServerDS.callProcedure(config.sgsServer, procedureName, parameters);
}

exports.callProcedure = callProcedure;
exports.sendCommand   = sendCommand;
exports.TYPES 		  = baseSqlServerDS.TYPES;