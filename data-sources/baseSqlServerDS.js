var debug 		= require('debug')('my-orbe:sqlServerDS');
var tedious		= require('tedious');
var Q 			= require('q');
var appRootPath = require('app-root-path');

var config 		= appRootPath.require('./shared/config');

var Connection 	= tedious.Connection;
var Request 	= tedious.Request;

function connect(endpoint) {

	debug('connecting...');

	var deferred = Q.defer();

  	var connection = new Connection(endpoint);

  	connection.on('connect', function(err) {

  		if(err) {

  			debug('error in connect: %s', err);
  			return deferred.reject(err);
  		}

		return deferred.resolve(connection);
    });

    return deferred.promise;
};

function parseRows(rows) {

	debug('parsing rows...');

	var result = [];

	rows.forEach(function(columns) {

		var parsedRow = {};

		columns.forEach(function(column) {

			parsedRow[column.metadata.colName] = column.value;
		});

		result.push(parsedRow);
	});

	return result;
};

function sendRequest(connection, command, parameters) {

	debug('sending request...');

	var deferred = Q.defer();

	var request = new Request(command, function(err, rowCount, rows) {

		if(err) {

			debug('error in sendRequest: %s', err);
			return deferred.reject(err);
		}

		var result = parseRows(rows);
		deferred.resolve(result);

		connection.close();
	});

	if(parameters) {

		debug('parameters: %j', parameters);

		parameters.forEach(function(parameter) {

			request.addParameter(parameter.name, parameter.type, parameter.value);
		});
	}

	connection.execSql(request);

	return deferred.promise;
};

function sendCommand(endpoint, command, parameters) {

	debug('sending command...');

	var send = function(connection) {

		return sendRequest(connection, command, parameters);
	};

	return connect(endpoint).then(send);
}

function callProcedureRequest(connection, procedureName, parameters) {

	debug('sending request...');

	var deferred = Q.defer();

	var result;

	connection.on('error', function(err) {

		debug('Closing connection with error: %j', err.message);

		return deferred.reject({ status: 400, message : err.message || 'Erro ao executar o procedimento no banco' });
	});

	var request = new Request(procedureName, function(err, rowCount, rows) {

		if(err) {

			debug('error in callProcedureRequest: %j', err.message);
			connection.close();
			return deferred.reject({ status : 400, message : err.message });
		}

		result = parseRows(rows);

		connection.close();

		deferred.resolve(result);
	});

	if(parameters) {

		debug('parameters: %j', parameters);

		parameters.forEach(function(parameter) {
			debug('adding parameter: %j', parameter);
			request.addParameter(parameter.name, parameter.type, parameter.value);
		});
	}

	connection.callProcedure(request);

	return deferred.promise;
};

function callProcedure(endpoint, procedureName, parameters) {

	var send = function(connection) {

		return callProcedureRequest(connection, procedureName, parameters);
	};

	return connect(endpoint).then(send);
}


exports.callProcedure = callProcedure;
exports.sendCommand   = sendCommand;
exports.TYPES 		  = tedious.TYPES;