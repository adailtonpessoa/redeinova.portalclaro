var arp         = require('app-root-path');
var debug 		= arp.require('./shared/debug')('myOrbeDS');
var q 	  		= require('q');
var rp 			= require('request-promise');
var config 		= arp.require('./shared/config');

var _uri = config.myOrbeURL;

function getAllLicensesForMyOrbeUser(signature) {

	debug('getting all licenses for my orbe user...');

	var options = {
	    uri: _uri + 'licencas',
	    headers: {
	        'Authorization': 'Signature ' + signature
	    },
	    json: true,
	    simple: false
	};

	return rp(options);
}

function listDynamicResourcesForModule() {
	debug('getting all endpoints for module: %j', config.moduleId);

	var options = {
	    uri: _uri + 'recursos/modulo/' + config.moduleId,
	    headers: {},
	    json: true,
	    simple: false
	};

	return rp(options);
}


function listDataSources(){
	debug('getting all datasources for Module: %j', config.moduleId);

	var options = {
	    uri: _uri + 'data-sources/modulo/' + config.moduleId,
	    headers: {},
	    json: true,
	    simple: false
	};

	return rp(options);
}

function listUserShare(){
	debug('getting all datasources for Module: %j', config.moduleId);

	var options = {
		uri: _uri + 'quota/modulo/' + config.moduleId,
		headers: {},
		json: true,
		simple: false
	};

	return rp(options);
}

function auth(usuario, senha){
	debug('Authenticating user %j %j', usuario, senha);

	var options = {
        method : 'POST',
	    uri: _uri + 'autenticar',
	    headers: {},
	    body : {
	    	login : usuario,
	    	senha : senha
	    },
	    json: true,
	    //simple: false
	};

	return rp(options);
}



exports.getAllLicensesForMyOrbeUser 	= getAllLicensesForMyOrbeUser;
exports.listDynamicResourcesForModule 	= listDynamicResourcesForModule;
exports.listDataSources 				= listDataSources;
exports.listUserShare 					= listUserShare;
exports.auth 							= auth;
