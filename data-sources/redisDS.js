var redis 		= require('redis');
var q			= require('q');
var config 		= require('app-root-path').require('./shared/config');

exports.getClient = function getClient() {

	var options = {}; 
	return redis.createClient(config.redis.port, config.redis.host, options);
};