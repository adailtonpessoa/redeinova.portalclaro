var arp		= require('app-root-path');
var debug 	= arp.require('./shared/debug')('sqlServerDS');
var tedious	= require('tedious');
var Q 		= require('q');
var config  = arp.require('./shared/config');
var util 	= require('util');

var Connection 	= tedious.Connection;
var Request 	= tedious.Request;

if(process.env.NODE_ENV == 'production') {

	config.userName = 'sa';
	config.password = 'jm#srv@1!v2';
	config.server 	= '50.16.197.87';
}

var connect = function(idProprietario) {

	debug('connecting...' + idProprietario);

	var deferred = Q.defer();

	var clienteConfig = config.getDataSource(idProprietario);

	if(!clienteConfig)
		return Q.reject({ status : 403, message : "Datasource não configurado adequadamente para o proprietário dessa licença"})

	var strCon = clienteConfig.conteudo;
	//if(!strCon.options.port)
	//	strCon.options.port = 1433;
	//if(!strCon.options.requestTimeout)
		strCon.options.requestTimeout = 90000;
	debug('connection string: %j, for owner: %j', strCon, idProprietario);
  	var connection = new Connection(strCon);

  	connection.on('connect', function(err) {

  		if(err) {

  			debug('error in connect: %s', err);
  			return deferred.reject({ status : 500, message : "Erro ao conectar ao banco do proprietário da licença"});
  		}

		return deferred.resolve(connection);
    });

    return deferred.promise;
};

var parseRows = function(rows) {

	debug('parsing rows...');

	var result = [];

	rows.forEach(function(columns) {

		var parsedRow = {};

		columns.forEach(function(column) {

			parsedRow[column.metadata.colName] = column.value;
		});

		result.push(parsedRow);
	});

	return result;
};

var sendRequest = function(connection, command, parameters) {

	debug('sending request...');

	var deferred = Q.defer();

	var result;

	connection.on('error', function(err) {
		debug('Closing connection with error: %j', err.message);
		err.severe = true;
		//if(err)
			return deferred.reject(err);
		//deferred.resolve(result);
	});

	var request = new Request(command, function(err, rowCount, rows) {

		if(err) {

			debug('error in sendRequest: %j', err.message);
			err.severe = true;
			connection.close();
			return deferred.reject({ status : 400, message : err.message });
		}

		result = parseRows(rows);

		connection.close();

    	deferred.resolve(result);
	});

	if(parameters) {

		debug('parameters: %j', parameters);

		parameters.forEach(function(parameter) {
			debug('adding parameter: %j', parameter);
			request.addParameter(parameter.name, parameter.type, parameter.value);
		});
	}

	connection.execSql(request);

	return deferred.promise;
};

var sendCommand = function(command, parameters, idProprietario) {

	debug('sending command... %j, parameters: %j', command, parameters);

	var send = function(connection) {

		return sendRequest(connection, command, parameters);
	};

	return connect(idProprietario)
			.then(send);
}

function callProcedureRequest(connection, procedureName, parameters) {

	debug('sending request...');

	var deferred = Q.defer();

	var result;

	connection.on('error', function(err) {
		debug('Closing connection with error: %j', err);
		err.severe = true;
		return deferred.reject(err);
	});

	var request = new Request(procedureName, function(err, rowCount, rows) {

		if(err) {

			debug('error in sendRequest: %j', err.message);

			connection.close();
			err.severe = true;

			return deferred.reject(err);
		}

		result = parseRows(rows);

		connection.close();

		deferred.resolve(result);
	});

	if(parameters) {

		//debug('parameters: %j', parameters);

		parameters.forEach(function(parameter) {
			//debug('adding parameter: %j', parameter);
			if(parameter.type.name == tedious.TYPES.Money.name){
				request.addParameter(parameter.name, parameter.type, parameter.value,{
					scale : 8, precision : 18
				});
			}
			else
				request.addParameter(parameter.name, parameter.type, parameter.value);
		});
	}

	connection.callProcedure(request);

	return deferred.promise;
};

function callProcedure(procedureName, parameters, idProprietario){

	var send = function(connection) {
		return callProcedureRequest(connection, procedureName, parameters);
	};

	return connect(idProprietario)
		.then(send);
}


exports.callProcedure = callProcedure;
exports.sendCommand = sendCommand;
exports.TYPES 		= tedious.TYPES;

// function executeStatement() {

//     var request = new Request("SELECT * from [dbo].[Usuario] where Logon='ivo.outros'", function(err, rowCount, rows) {

// 	    if (err) return console.log(err);

// 		console.log(rowCount + ' rows returned');

// 		console.log( parseRows(rows) );
//     });

//     _connection.execSql(request);
// };

//exports.connect = connect;