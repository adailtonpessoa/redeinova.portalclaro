var arp         = require('app-root-path');
var q           = require('q');
var sqlServer   = arp.require('./data-sources/sgsDS');
var debug       = arp.require('./shared/debug')('ParametroModel');


function ReportRS(){

    var self = this;

    this.getReport    = function(reportId){
        var parameters = [
            { name : 'Id', type : sqlServer.TYPES.Int, value : reportId }
        ];
        var sql = "select r.*, sr.Dominio, sr.URL, sr.Usuario from Relatorio r join RelatorioGrupo rg on r.IdRelatorioGrupo = rg.Id  join ServidorRelatorio sr on sr.Id = rg.IdServidorRelatorio where r.Id = @Id";
        return self.sendCommand(sql, parameters);
    };
    
}

ReportRS.prototype = sqlServer;

module.exports = new ReportRS();
