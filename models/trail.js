var appRootPath = require('app-root-path');
var mongoose 	= require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
	idUsuarioProprietario: Number,
	idUsuarioOperador: Number,
	url: String,
	metodoHttp: String,
	statusDaResposta: Number,
	mensagemDeErro: String,
	tempoDeResposta : Number,
	tamanhoDaRequisicao: Number,
	tamanhoDaResposta: Number,
	data: { type: Date, default: Date.now },
	ip: String,
	moduloId: Number,
	ipAPI: String
});

schema.index({ idUsuarioOperador: 1, data: -1 });

var Trail = mongoose.model('rastro', schema);

module.exports = Trail;
