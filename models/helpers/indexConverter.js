var debug = require('app-root-path').require('./shared/debug')('indexConverter');

var convertMongoQueryInIndexToRedisKey = function(modelNameInRedis, mongoQueryInIndex) {

	debug("converting mongo's query in index to redis key...");

	var keyBuilder = [];

	for(var i in mongoQueryInIndex)
		keyBuilder.push(i + ':' + mongoQueryInIndex[i]);

	return modelNameInRedis + ':' + keyBuilder.join(':');
};

var convertToSetListName = function(modelNameInRedis, fieldName) {

	return modelNameInRedis + ':' + fieldName + 's';
};

exports.convertMongoQueryInIndexToRedisKey 	= convertMongoQueryInIndexToRedisKey;
exports.convertToSetListName 				= convertToSetListName;