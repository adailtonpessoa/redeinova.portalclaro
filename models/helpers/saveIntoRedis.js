var appRootPath	   = require('app-root-path');
var debug		   = appRootPath.require('./shared/debug')('saveIntoRedis');
var redisDS		   = appRootPath.require('./data-sources/redisDS');
var indexConverter = appRootPath.require('./models/helpers/indexConverter');

module.exports = function saveIntoRedis(modelInstance, modelNameInRedis, indexesArray) {

	debug('starting...');

	var serializedData = JSON.stringify(modelInstance);

	var redisClient = redisDS.getClient();
	redisClient.unref();

	var multiCommand = redisClient.multi();

	//generating Redis index(es)
	indexesArray.forEach(function(index) {

		var key = indexConverter.convertMongoQueryInIndexToRedisKey(modelNameInRedis, index);
		multiCommand.set(key, serializedData);
	});

	//saving
	multiCommand.exec(function(error, replies) {

		if(error) return debug('error: %s', error);

		replies.forEach(function(reply, index) {

			debug('reply ' + index + ': %s', reply);
		});

		debug('done');;
	});
};