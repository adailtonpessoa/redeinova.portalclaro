var appRootPath	   = require('app-root-path');
var debug		   = appRootPath.require('./shared/debug')('retrieveSetListFromRedis');
var Q 			   = require('q');
var redisDS		   = appRootPath.require('./data-sources/redisDS');
var indexConverter = appRootPath.require('./models/helpers/indexConverter');

module.exports = function retrieveSetListFromRedis(fieldName, modelNameInRedis) {

	debug('starting...');

	var redisClient = redisDS.getClient();
	redisClient.unref();

	var setListName = indexConverter.convertToSetListName(modelNameInRedis, fieldName);

	return Q.ninvoke(redisClient, 'smembers', setListName);
};