var appRootPath	   = require('app-root-path');
var debug		   = appRootPath.require('./shared/debug')('findAllByIndexFromSetList');
var Q			   = require('q');


//Model Helpers
var _retrieveSetListFromRedis	= appRootPath.require('./models/helpers/retrieveSetListFromRedis');
var _findAllByIndex 			= appRootPath.require('./models/helpers/findAllByIndex');

module.exports = function findAllByIndexFromSetList(mongooseContext, fieldName, modelNameInRedis) {

	debug('starting...');

	var generateQueryInIndex = function(ids) {

		debug('generating query in index...');

		var queryInIndexArray = ids.map(function(id) {

			return { _id: id };
		});

		return queryInIndexArray;
	};

	var findAll = function(queryInIndexArray) {

		debug('finding all...');

		return _findAllByIndex(mongooseContext, modelNameInRedis, queryInIndexArray);
	};

	return _retrieveSetListFromRedis('_id', 'routenodes');
};