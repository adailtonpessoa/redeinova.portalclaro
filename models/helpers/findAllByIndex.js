var appRootPath	   = require('app-root-path');
var debug		   = appRootPath.require('./shared/debug')('findByIndex');
var Q			   = require('q');


//Model Helpers
var _findByIndex = appRootPath.require('./models/helpers/findByIndex');

//Shared
var _groupSpreadResults = appRootPath.require('./shared/groupSpreadResults');

module.exports = function findAllByIndex(mongooseContext, modelNameInRedis, queryInIndexArray) {

	debug('starting...');

	var findAll = function() {

		debug('finding all...');

		var promises = [];

		queryInIndexArray.forEach(function(queryInIndex) {

			var promise = _findByIndex(mongooseContext, modelNameInRedis, queryInIndex);
			promises.push(promise);
		});

		return Q.all(promises);
	};

	var groupResultsInOneArray = function() {

		return _groupSpreadResults(arguments);
	};

	return findAll().spread(groupResultsInOneArray);
};