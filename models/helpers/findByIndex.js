var appRootPath	   = require('app-root-path');
var debug		   = appRootPath.require('./shared/debug')('findByIndex');
var Q			   = require('q');
var redisDS		   = appRootPath.require('./data-sources/redisDS');
var indexConverter = appRootPath.require('./models/helpers/indexConverter');

module.exports = function findByIndex(mongooseContext, modelNameInRedis, queryInIndex, callback) {

	debug('starting...');

	var handlingError = function(error) {

		debug('handling error');
		debug('error: ', error);

		return Q.reject('Not found');
	};

	var findInRedis = function() {

		debug('finding in redis...');

		//This convertion handle with compound index too
		var key = indexConverter.convertMongoQueryInIndexToRedisKey(modelNameInRedis, queryInIndex);

		var redisClient = redisDS.getClient();
		redisClient.unref();

		return Q.ninvoke(redisClient, 'get', key);
	};

	var checkRedisResult = function(result) {

		debug('checking redis result...');

		if(result) {

			var obj = JSON.parse(result);

			debug('obj from redis: %j', obj);

			return mongooseContext.hydrate(obj);
		}

		return Q.fcall(findInMongo);
	};

	var findInMongo = function() {

		debug('finding in mongo...' + JSON.stringify(queryInIndex));

		return Q.ninvoke(mongooseContext, 'findOne', queryInIndex);
	};

	return findInRedis()
			.then(checkRedisResult)
			.catch(handlingError)
			.nodeify(callback);
};