var appRootPath	   = require('app-root-path');
var debug		   = appRootPath.require('./shared/debug')('addSetListMemberIntoRedis');
var redisDS		   = appRootPath.require('./data-sources/redisDS');
var indexConverter = appRootPath.require('./models/helpers/indexConverter');

module.exports = function addSetListMemberIntoRedis(modelInstanceField, fieldName, modelNameInRedis) {

	debug('adding set list member into redis...');

	var redisClient = redisDS.getClient();
	redisClient.unref();

	var setListName = indexConverter.convertToSetListName(modelNameInRedis, fieldName);

	redisClient.sadd(setListName, modelInstanceField, function(error, reply) {

		if(error) return debug('error: %s', error);

		debug('reply: %s', reply);
	});
};