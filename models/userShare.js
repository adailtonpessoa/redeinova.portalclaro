
var arp         = require('app-root-path');
var debug 		= arp.require('./shared/debug')('UserShare');
var mongoose 	= require('mongoose');


//Schema
var Schema      = mongoose.Schema;
var ObjectId    = Schema.Types.ObjectId;

var schema = new Schema({
    idProprietario : Number,
	vigencia : Date,
    recurso : String,
    limite : { quantidadeRequisicoes : Number, dataAtualizacao : Date },
	quantidadeRequisicoes : Number,
    dataEmQueExcedeu : Date,
    foiExcedido : Boolean,
    ativo : Boolean,
    resets : [ { quantidadeRequisicoes : Number, data : { type : Date, default : Date.now }} ]
});

schema.index( { idProprietario: 1, vigencia: 1 } );

schema.index( { idProprietario: 1, vigencia: 1, recurso : 1 } );

var UserShare = mongoose.model("UserShare", schema);

debug('UserShare model', UserShare);

module.exports = UserShare;