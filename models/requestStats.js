
var arp = require('app-root-path');
var debug 		= arp.require('./shared/debug')('RequestStats');
var mongoose 	= require('mongoose');


//Schema
var Schema      = mongoose.Schema;
var ObjectId    = Schema.Types.ObjectId;

var schema = new Schema({
	idProprietario : Number,
	idUsuarioRequisicao : Number,
	dataRequisicao : Date,
	recurso : String,
	requisicao : { tamanho : Number, conteudo : Object },
	resposta : { tamanho : Number, conteudo : Object }
});



//schema.statics.findByIdentifier = function(id, callback) {
//
//	return findByIndex(this, 'place', { _id: id }, callback);
//};



var RequestStats = mongoose.model("RequestStats", schema);

module.exports = RequestStats;