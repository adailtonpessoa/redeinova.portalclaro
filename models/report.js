var arp         = require('app-root-path');
var q           = require('q');
var sqlServer   = arp.require('./data-sources/sqlServerDS');
var debug       = arp.require('./shared/debug')('ReportModel');


function Report(){

    var self = this;

    this.listReports    = function(id_proprietario, login){
        var parameters = [
            { name : 'login', type : sqlServer.TYPES.VarChar, value : login }
        ];
        return this.callProcedure('sp_locus_listar_relatorios', parameters, id_proprietario);
    };

    this.listReportColumns  = function(id_proprietario, login){
        var parameters = [
            { name : 'login', type : sqlServer.TYPES.VarChar, value : login }
        ];
        return this.callProcedure('sp_locus_listar_relatorios_colunas', parameters, id_proprietario);
    };

    this.listReportParams   = function(id_proprietario, login){
        var parameters = [
            { name : 'login', type : sqlServer.TYPES.VarChar, value : login }
        ];

        function getParamListDefault (paramList){
            var paramsWithDefault = paramList.filter(function(param){
                return (param.valor_default + '') .trim().indexOf('select') == 0;
            });

            var getParamAllParamsDefault = paramsWithDefault.map(function(param){
                return getParamDefault(param);
            });

            return q.all(getParamAllParamsDefault)
                .then(function(){
                    return q(paramList);
                });
        }

        function getParamDefault(param){
            debug('default: %j', param.valor_default);
            return self.sendCommand(param.valor_default, [], id_proprietario)
            .then(function(res){
                param.valor_default = res[0];
                return q();
            });
        }

        return this.callProcedure('sp_locus_listar_relatorios_parametros', parameters, id_proprietario)
        .then(getParamListDefault);
    };

    this.listReportParamsList  = function(id_proprietario, login, id_relatorio, id_param){

        function obterEmpresa(){
            var sql = "select empresa from supervisores where [login] = @login";
            var params = [{
                name : 'login',
                type : sqlServer.TYPES.VarChar,
                value : login
            }];
            return self.sendCommand(sql, params, id_proprietario);
        }


        function obterConsulta(rows){
            if(rows.length == 0)
                return q.reject({ status : 401, message : 'Usuário não é supervisor '});

            var empresa = rows[0].empresa;
            debug('Empresa: ');
            debug(empresa);

            var sql = "select pesquisar_onde from relatorios_parametros where sequencial = @sequencial and relatorio = @relatorio";

            var params = [
                {
                    name : 'relatorio',
                    type : sqlServer.TYPES.Int,
                    value : id_relatorio
                },
                {
                    name : 'sequencial',
                    type : sqlServer.TYPES.Int,
                    value : id_param
                },
                {
                    name : 'empresa',
                    type : sqlServer.TYPES.Int,
                    value : empresa
                }
            ];
            return self.sendCommand(sql, params, id_proprietario)
            .then(function(rows){
                if(rows.length == 0)
                    return q.reject({ status : 401, message : 'Parâmetro não encontrado'});
                return q({
                   empresa ,  sql : rows[0].pesquisar_onde
                });
            });
        }

        function executarConsulta(opts){

            var sql = opts.sql;
            var params = [
                {
                    name : 'empresa',
                    type : sqlServer.TYPES.Int,
                    value : opts.empresa
                }
            ];

            return self.sendCommand(sql, params, id_proprietario);
        }

        return obterEmpresa()
        .then(obterConsulta)
        .then(executarConsulta);


    };

    this.searchValueByTerm         = function(id_proprietario, login, id_relatorio, id_param, term){
        function obterEmpresa(){
            var sql = "select empresa from supervisores where [login] = @login";
            var params = [{
                name : 'login',
                type : sqlServer.TYPES.VarChar,
                value : login
            }];
            return self.sendCommand(sql, params, id_proprietario);
        }


        function obterConsulta(rows){
            if(rows.length == 0)
                return q.reject({ status : 401, message : 'Usuário não é supervisor '});

            var empresa = rows[0].empresa;
            debug('Empresa: ');
            debug(empresa);

            var sql = "select pesquisar_onde from relatorios_parametros where sequencial = @sequencial and relatorio = @relatorio";

            var params = [
                {
                    name : 'relatorio',
                    type : sqlServer.TYPES.Int,
                    value : id_relatorio
                },
                {
                    name : 'sequencial',
                    type : sqlServer.TYPES.Int,
                    value : id_param
                },
                {
                    name : 'empresa',
                    type : sqlServer.TYPES.Int,
                    value : empresa
                }
            ];
            return self.sendCommand(sql, params, id_proprietario)
                .then(function(rows){
                    if(rows.length == 0)
                        return q.reject({ status : 401, message : 'Parâmetro não encontrado'});
                    return q({
                        empresa ,  sql : rows[0].pesquisar_onde
                    });
                });
        }

        function executarConsulta(opts){

            var sql = opts.sql;

            sql = "select codigo, descricao from ( " + sql;
            var params = [
                {
                    name : 'empresa',
                    type : sqlServer.TYPES.Int,
                    value : opts.empresa
                }
            ];

            var iTerm = parseInt(term);

            if(isNaN(iTerm)){
                params.push({
                    name : 'descricao',
                    type : sqlServer.TYPES.VarChar,
                    value:  '%' + term + '%'
                });
                sql += ") t where t.descricao like @descricao ";
            }
            else{
                params.push({
                    name : 'codigo',
                    type : sqlServer.TYPES.Int,
                    value: iTerm
                });
                sql += ") t where t.codigo = @codigo ";
            }



            return self.sendCommand(sql, params, id_proprietario);
        }

        return obterEmpresa()
            .then(obterConsulta)
            .then(executarConsulta);


    };


    this.getReportData         = function(id_proprietario, login, id_relatorio, filters){

        function obterEmpresa(){
            var sql = "select empresa from supervisores where [login] = @login";
            var params = [{
                name : 'login',
                type : sqlServer.TYPES.VarChar,
                value : login
            }];
            return self.sendCommand(sql, params, id_proprietario);
        }


        function obterConsulta(rows){
            if(rows.length == 0)
                return q.reject({ status : 401, message : 'Usuário não é supervisor '});

            var empresa = rows[0].empresa;
            debug('Empresa: ');
            debug(empresa);

            var sql = "select comando_sql from relatorios where sequencial = @relatorio";

            var params = [
                {
                    name : 'relatorio',
                    type : sqlServer.TYPES.Int,
                    value : id_relatorio
                }
            ];
            return self.sendCommand(sql, params, id_proprietario)
                .then(function(rows){
                    debug('Reports found %j', rows);
                    if(rows.length == 0)
                        return q.reject({ status : 401, message : 'Parâmetro não encontrado'});
                    return q({
                        empresa ,  sql : rows[0].comando_sql
                    });
                });
        }

        function executarConsulta(opts){

            var sql = opts.sql;
            debug('Filtros: %j', filters);

            //sql = "select codigo, descricao from ( " + sql;
            var params = [
                {
                    name : 'empresa',
                    type : sqlServer.TYPES.Int,
                    value : opts.empresa
                },
                {
                    name : 'internal_report_usuario',
                    type : sqlServer.TYPES.VarChar,
                    value : login
                }
            ];

            for (var i = 0; i < filters.length; i++)
            {
                if (filters[i].tipo == "M")
                {
                    var valor = '';
                    if(filters[i].valor){
                        valor = 'X';
                    }
                    else{
                        filters[i].valor = '';
                        valor = '-';
                    }

                    params.push({
                        name : 'internal_report_gen' + (i + 1),
                        type : sqlServer.TYPES.VarChar,
                        value : valor
                    });
                }
            }

            sql = sql.replace(/\{0}/, opts.empresa);
            filters.sort((f1,f2)=>{ return f1.sequencial - f2.sequencial; });
            for(var k = 0; k < filters.length;k++){
                var regex = new RegExp("\\{" + (k + 1) + "}", "g");
                sql = sql.replace(regex, filters[k].valor);
            }

            debug("Report data sql: ");
            debug(sql);

            return self.sendCommand(sql, params, id_proprietario);
        }

        return obterEmpresa()
            .then(obterConsulta)
            .then(executarConsulta);


    };

}


Report.prototype = sqlServer;



module.exports = new Report();
