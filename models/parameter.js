var arp         = require('app-root-path');
var q           = require('q');
var sqlServer   = arp.require('./data-sources/sqlServerDS');
var debug       = arp.require('./shared/debug')('ParametroModel');


function Parameter(){

    var self = this;

    this.getParameter    = function(id_proprietario, empresa, nomeParametro){
        var parameters = [
            { name : 'parametro', type : sqlServer.TYPES.VarChar, value : nomeParametro },
            { name : 'empresa', type : sqlServer.TYPES.Int, value : (empresa || 0) }
        ];
        var sql = "select top 1 * from parametros where empresa = @empresa and parametro = @parametro";
        return self.sendCommand(sql, parameters, id_proprietario);
    };


}

Parameter.prototype = sqlServer;

module.exports = new Parameter();
