var arp 		= require('app-root-path');
var q			= require('q');
var Report      = arp.require('./models/reportrs');
var Parameter      = arp.require('./models/parameter');
var debug 		= arp.require('./shared/debug')('Dashboard');
var http            = require('http-request');
var utils		= arp.require('./shared/utils');
var config		= arp.require('./shared/config');


module.exports =  function(request, response){
  
    function getTrustedTicket(){
        
        debug('Listing params');
        
        //var reqBody = "username=" +  request.payload.usuario.login;
        var reqBody = "username="+request.payload.usuario.login;

        var deferred = q.defer();
        
        http.post(
            {
                url: config.resolutoServerURL,
                reqBody: new Buffer(reqBody),
                headers: {
                    'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
                }
            }, function (err, res) {
                if (err) {
                    console.error(err);
                    return deferred.reject({ status : 400, message : err});
                }
                res = res.buffer.toString();
                if(res == "-1")
                    return deferred.reject({ status : 400, message : 'Relatório não está liberado no servidor.'});
                deferred.resolve(res);
            } 
        );
        return deferred.promise;

    }

    function sendResponse(res){

        response.status(200).send({
            ticket : res
        });
    }

    getTrustedTicket()
    .then(sendResponse)
    .catch(function(err){
        console.log(err);
        response.status(400).send(err);
    });
};