var arp 		= require('app-root-path');
var q			= require('q');
var Report      = arp.require('./models/report');
var debug 		= arp.require('./shared/debug')('Report');
var utils		= arp.require('./shared/utils');
var config		= arp.require('./shared/config');


module.exports =  function(request, response){


    function getReportListValues(){
        debug('Listing reports');

        debug(request.payload.usuario);
        return Report.listReportParamsList(request.license.usuarioProprietario.id, request.payload.usuario.login,
            request.params.id_report, request.params.id_param);
    }

    function sendResponse(data){
        response.status(200).send(data);
    }

    getReportListValues()
        .then(sendResponse)
        .catch(utils.handleError(response));
};