var arp 		= require('app-root-path');
var q			= require('q');
var Report      = arp.require('./models/report');
var debug 		= arp.require('./shared/debug')('Report');
var utils		= arp.require('./shared/utils');
var config		= arp.require('./shared/config');


module.exports =  function(request, response){


    function getReportList(){
        debug('Listing reports');
        debug(request.payload.usuario);
        return Report.listReports(request.license.usuarioProprietario.id, request.payload.usuario.login);
    }

    function getColumnsList(reportList){
        debug('Listing columns');
        return Report.listReportColumns(request.license.usuarioProprietario.id, request.payload.usuario.login)
        .then(function(columns){
            return q({
                reports : reportList,
                columns : columns
            });
        });
    }

    function getParamsList(reports){
        debug('Listing params');
        return Report.listReportParams(request.license.usuarioProprietario.id, request.payload.usuario.login)
            .then(function(params){
                reports.params = params;
                return q(reports);
            });
        return q(reports);
    }

    function sendResponse(data){
        response.status(200).send(data);
    }

    getReportList()
    .then(getColumnsList)
    .then(getParamsList)
    .then(sendResponse)
    .catch(utils.handleError(response));
};