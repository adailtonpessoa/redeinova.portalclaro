var arp 		= require('app-root-path');
var q			= require('q');
var Report      = arp.require('./models/report');
var debug 		= arp.require('./shared/debug')('Report');
var utils		= arp.require('./shared/utils');
var config		= arp.require('./shared/config');


module.exports =  function(request, response){

    function validate(){
        if(!(request.body instanceof Array)){
            return q.reject({ status : 400, message : 'Parâmetros em formato inválido'});
        }
        return q();
    }

    function getReportListValues(){
        debug('Listing reports');

        return Report.getReportData(request.license.usuarioProprietario.id, request.payload.usuario.login,
            request.params.id_report, request.body);
    }

    function sendResponse(data){
        response.status(200).send(data);
    }

    validate()
    .then(getReportListValues)
    .then(sendResponse)
    .catch(utils.handleError(response));
};
