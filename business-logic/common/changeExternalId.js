
var arp  = require('app-root-path');

var debug 	= arp.require('./shared/debug')('changeExternalId');
var Q		= require('q');

module.exports = function changeExternalId(entity, externalId) {

	debug('doing...');

	entity.external_id = externalId == null ? '' : externalId;
	return Q(entity);
};