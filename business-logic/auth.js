var arp 		= require('app-root-path');
var q			= require('q');
var myOrb 		= arp.require('./data-sources/myOrbeDS'); 

var debug 		= arp.require('./shared/debug')('business-rules_auth');
var utils		= arp.require('./shared/utils');
var config		= arp.require('./shared/config');


module.exports =  function(request, response){

	debug("Request to auth");

	function validateRequest()
	{
		debug("Validating request");

		if(!request.body.login)
			return q.reject( { status : 400, message : 'Login deve ser fornecido' });
		if(!request.body.senha)
			return q.reject( { status : 400, message : 'Senha deve ser fornecida' });
		debug("Past validation");

		return q();
	}

	function authUser()
	{
		debug('auth in myorb');
		return myOrb.auth(request.body.login, request.body.senha);
	}

	function checkLicense(signature){
		//debug('signature: ' + signature);
		return myOrb.getAllLicensesForUserOrbeId(signature)
            .then(function(modules){
                return q({ modules : modules, signature : signature});
            });
	}

	function validateLicenses(res){
		debug('Licencas: %j', res.modules);
		if(res.modules.some(l => l.modulo.id == config.moduleId))
		{
			debug('Acesso ao módulo');
			return q(res.signature);
		}
		debug('Sem acesso ao módulo');
		return q.reject({ status : 401, message :  'Usuário não tem permissão para acessar o sistema' });
	}

	function sendResponse(signature){
		response.status(200).send(signature);
	}

	validateRequest()
	.then(authUser)
	.then(checkLicense)
	.then(validateLicenses)
	.then(sendResponse)
	.catch(utils.handleError(response));
};