var arp 	= require('app-root-path');
var config 	= arp.require('./shared/config');
var debug 	= require('debug');

module.exports = function(fileName) {

	return debug(config.moduleName + ':' + fileName);
}