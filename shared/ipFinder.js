var os = require('os');

function find() {

  var ifaces  = os.networkInterfaces();
  var ifnames = Object.keys(ifaces);
  var ips     = [];

  ifnames.forEach(function (ifname) {

    var alias = -1;

    ifaces[ifname].forEach(function (iface) {

      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      if ('IPv4' !== iface.family || iface.internal !== false)
        return;

      ++alias;

      // this single interface has multiple ipv4 addresses
      if (alias >= 1) {

        var name    = ifname + ':' + alias;
        var address = iface.address;

        console.log(ifname + ':' + alias, iface.address);

        ips.push({ name: name, address: address });

        return;
      }

      // this interface has only one ipv4 adress
      console.log(ifname, iface.address);

      ips.push({ name: ifname, address: iface.address });
    });

  });

  return ips;
}

exports.find = find;
