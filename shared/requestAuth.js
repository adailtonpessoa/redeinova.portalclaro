//Modules
var UAParser    = require('ua-parser-js');
var appRootPath = require('app-root-path');
var debug       = appRootPath.require('./shared/debug')('requestAuth');
var token       = appRootPath.require('/shared/token');
var signature   = appRootPath.require('/shared/signature');

var allowedIps = ['177.65.102.174','187.18.189.104',
'179.156.172.178', '23.97.96.32', '23.97.102.53', '191.190.73.38', '127.0.0.1'];
var allowedSubIp = '23.97';

function generateSignature(request, secretInfo, payload, callback) {

	if(!callback && typeof payload !== 'object') {

        callback = payload;
		payload = {};
	}

    var checkings = [
        { target: request, message: 'Request can not be null or undefined' },
        { target: secretInfo, message: 'Missing secretInfo' },
        { target: secretInfo ? secretInfo.ticket : null, message: 'Missing ticket' },
        { target: secretInfo ? secretInfo.clientId: null, message: 'Missing clientId' },
        { target: secretInfo ? secretInfo.userId: null, message: 'Missing userId' },
        { target: secretInfo ? secretInfo.moduleId: null, message: 'Missing moduleId' },
    ];

    for(var i=0; i<checkings.length; i++) {

        var checking = checkings[i];

        if(!checking.target) {

            callback(checking.message);
            return;
        }
    }

    generate(request, secretInfo, payload, callback);
}

function generate(request, secretInfo, payload, callback) {

    debug('request headers: %j', request.headers);

    var origin = getOrigin(request);
    var ip     = getIp(request);

    secretInfo.origin = origin;
    secretInfo.ip = ip;

    payload.token = token.encode(secretInfo);

    signature.generate(payload, function(err, sign) {

        if(err) callback(err, null);
        else    callback(null, sign)
    });
}

function getOrigin(request) {

	var parser = new UAParser();
    parser.setUA(request.headers['user-agent']);

	var origin = {
		browser: parser.getBrowser(),
		device: parser.getDevice(),
		os: parser.getOS()
	};

    debug('origin: %j', origin);

    return JSON.stringify(origin);
}

function getIp(request) {

    debug('ip: %j', request.connection.remoteAddress);

    var pieces = request.connection.remoteAddress.split(':');

    return pieces[pieces.length -1];
}

function getPayloadFromSignature(sign) {

	var signatureParts = sign.split('.');
	var buffer = new Buffer(signatureParts[1], 'base64');
	var payloadString = buffer.toString();

	return JSON.parse(payloadString);
}

function authenticate(request, callback) {

    if(request.headers['authorization']) {

        var sign = request.headers['authorization'].split(' ')[1];

        signature.verify(sign, function (err, payload) {

            if (!err) {

                var t = payload.token;
                var secretInfo = token.decode(t);

                var currentOrigin = getOrigin(request);
                var currentIp = getIp(request);

                // if (allowedIps.indexOf(currentIp) == -1 && currentIp.toString().indexOf(allowedSubIp) != 0 &&
                // (JSON.stringify(secretInfo.origin) !== currentOrigin || secretInfo.ip !== currentIp)) {

                //     var message = 'Current ip or origin are not the same from authorization time. IPs: (original) ' + secretInfo.ip + ' - (current) ' + currentIp + ' |  Origins: (original) '+ JSON.stringify(secretInfo.origin) + ' - (current) ' + currentOrigin;
                //     callback(message);
                // }
                // else callback(null, { payload: payload, secretInfo: secretInfo });
                callback(null, { payload: payload, secretInfo: secretInfo });
            }
            else callback(err);
        });
    }
    else callback('No authorization header');
}

exports.generateSignature = generateSignature;
exports.getPayloadFromSignature = getPayloadFromSignature;
exports.authenticate = authenticate;