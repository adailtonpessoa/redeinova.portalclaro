/**
 * Created by davisabino on 26/02/16.
 */
var arp = require('app-root-path');
var config = arp.require('./shared/config');
var debug = arp.require('./shared/debug')('SimpleCache');

function SimpleCache(){
    var _cache = {};
    this.getFromCache = function(key){
        debug('Getting from cache');
        if(key && _cache[key])
            return _cache[key].value;
        return null;
    };

    this.addToCache = function(key, value, expTime, handleRemoval){
        debug('adding %j to cache with key %j', value, key);

        if(!handleRemoval && typeof(expTime) == 'function'){
            handleRemoval = expTime;
            expTime = null;
        }

        if(key){
            var _cachedVal = _cache[key];

            if(_cachedVal && _cachedVal.hitCount < config.cacheRequestHit){
                cancelTimeout(_cachedVal.timeOutId);
                _cachedVal = { value :  value, hitCount : _cachedVal.hitCount };
                _cache[key] = _cachedVal;
                _cachedVal.timeOutId = setTimeout(function(){
                    debug('Removendo do cache ' + key);
                    delete _cache[key];
                    if(handleRemoval)
                        handleRemoval(_cachedVal.value);
                }, (expTime || config.cacheTimeout) * 1000);
            }
            else
            {
                _cachedVal = { value :  value, hitCount : 0 };
                _cache[key] = _cachedVal;
                _cachedVal.timeOutId = setTimeout(function(){
                    debug('Removendo do cache ' + key);
                    delete _cache[key];
                    if(handleRemoval)
                        handleRemoval(_cachedVal.value);
                }, (expTime || config.cacheTimeout) * 1000);
            }
            if(_cachedVal)
                _cachedVal.hitCount++;
        }
    };
};

var simpleCache = new SimpleCache();

module.exports = simpleCache;