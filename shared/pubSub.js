var appRootPath = require('app-root-path');
var redisDS 	= appRootPath.require('./data-sources/redisDS');
var debug 		= appRootPath.require('./shared/debug')('pubSub');
var config 		= appRootPath.require('./shared/config');

var redisEmitter 	= redisDS.getClient();
var redisListener 	= redisDS.getClient();

var subscribers = [];

function publish(message) {

	message.apiName 	= config.moduleName;
	message.apiModuleId = config.moduleId;
	message 			= JSON.stringify(message);

	debug('publishing...');
	debug('channel: %s - message: %s', config.pubSubChannel, message);

	redisEmitter.publish(config.pubSubChannel, message);
}

function subscribeForChannel(channel) {

	debug('subscribing for channel...');
	debug('channel: %s', channel);

	var isChannelAlreadySubscribed = subscribers.some(subscriber => subscriber.channel === channel);

	if(!isChannelAlreadySubscribed)
		redisListener.subscribe(channel);
}

function subscribe(channel, operation, callback) {

	debug('subscribing...');
	debug('channel: %s - operation: %s', channel, operation);

	subscribeForChannel(channel);

	var subscriber = {
		id: subscribers.length,
		channel: channel,
		operation: operation,
		callback: callback
	};

	subscribers.push(subscriber);

	return subscriber.id;
}

function unsubscribe(id) {

	debug('unsubscribing...');
	debug('id: %s', id);
	debug('total subscribers: %s', subscribers.length);

	var channel = subscribers[id].channel;

	subscribers.splice(id, 1);

	debug('updated total subscribers: %s', subscribers.length);

	var hasChannelAtLeastOneSubscriber = subscribers.some(subscriber => subscriber.channel === channel);

	if(!hasChannelAtLeastOneSubscriber)
		redisListener.unsubscribe(channel);
}

function handleMessage(channel, message) {

	debug('handling message...');
	debug('channel: %s', channel);
	debug('operation: %s', message);

	message 			= JSON.parse(message);
	var operation		= message.operation;

	var targets = subscribers.filter(subscriber => subscriber.channel === channel &&
		(subscriber.operation === '*' || subscriber.operation === operation));

	debug('total targets: %s', targets.length);

	targets.forEach(target => target.callback(message));

	debug('ending handling message');
}

redisListener.on('message', handleMessage);

exports.publish 			= publish;
exports.subscribe			= subscribe;
exports.unsubscribe 		= unsubscribe;