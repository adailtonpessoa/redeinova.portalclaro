var debug = require('app-root-path').require('./shared/debug')('groupSpreadResults');

module.exports = function groupSpreadResults(argumentsFromOther) {

	debug('grouping spread results...');

	var array = [];

	for(var i =0; i<argumentsFromOther.length; i++) {

		var result = argumentsFromOther[i];

		if(result)
			array.push(result);
	};

	return array;
};