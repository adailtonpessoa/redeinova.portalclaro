var arp = require('app-root-path');
var debug = arp.require('./shared/debug')('utils');
var sqlServerDS	= arp.require('./data-sources/sqlServerDS');
var nodeUtil	= require('util');


var bValues = ['true', 'false', '1', '0'];

function getSubType(parametroDaRequisicao){
	switch(parametroDaRequisicao.tipoNaRequisicaoAoBanco.toLowerCase()) {
		case "int":
			return sqlServerDS.TYPES.Int;
		case "decimal":
			return sqlServerDS.TYPES.Money;
		case "string":
			return sqlServerDS.TYPES.VarChar;
		case "date":
        case "datetime":
			return sqlServerDS.TYPES.DateTime;
		case "bool":
		case "boolean":
		case "bool":
			return sqlServerDS.TYPES.Bit;
	}
	return sqlServerDS.TYPES.VarChar;
}

module.exports.validateType = function(parametroDaRequisicao, queryParams){
	debug('QUERY: ');
	debug(queryParams);
	var requestVal = queryParams[parametroDaRequisicao.nomeNaRequisicaoHttp];
		var err = false;
	//parametroDaRequisicao.erro = "";
		switch(parametroDaRequisicao.tipoNaRequisicaoAoBanco.toLowerCase()) {
			case "int":
				var svalue = '' + requestVal;

				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Int;
				//if(!parametroDaRequisicao.requerido && !requestVal){
				//	if(parametroDaRequisicao.valorDefault)
				//		parametroDaRequisicao.valor = eval('(' + parametroDaRequisicao.valorDefault + ')');
				//	else
				//	{
				//		//parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Null;
				//		parametroDaRequisicao.valor = null;
				//	}
				//}
				//else
				if(requestVal) // || parametroDaRequisicao.requerido)
				{
					//debug("Type: %j, %j ", typeof(parseInt(requestVal)), Number.isSafeInteger(parseInt(requestVal)));
					var requestVal = parseInt(requestVal + '');

					if(isNaN(requestVal) || (requestVal + '').indexOf('.') > -1 || requestVal > 2147483647 || requestVal < -2147483648 || svalue.length > 10 || svalue.indexOf('e+') != -1)
					{
						err = "Tipo de dado incompatível: É esperado um valor numérico inteiro para o campo.";
					}
					else if(requestVal == null || eval('(' + requestVal + ')') == null)
					{
						//parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Null;
						parametroDaRequisicao.valor = null;
					}
					else
						parametroDaRequisicao.valor = requestVal;
				}
				else
				{
					//parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Null;
					parametroDaRequisicao.valor = null;
				}
			break;
			case "decimal":
			debug('Parsing parameter %j', parametroDaRequisicao);
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Decimal;
				//if(!parametroDaRequisicao.requerido && !requestVal)
			//	{
			//		debug('Atribuir valor default ou null');

			//		if(parametroDaRequisicao.valorDefault)
			//			parametroDaRequisicao.valor = eval('(' + parametroDaRequisicao.valorDefault + ')');
			//		else
			//			parametroDaRequisicao.valor = null;
			//	}
				//else
				if(requestVal)// || parametroDaRequisicao.requerido)
				{
					if(isNaN(requestVal))
					{
						err = "Tipo de dado incompatível: É esperado um valor numérico para o campo.";
					}
					else if(requestVal == null || eval('(' + requestVal + ')') == null){
						parametroDaRequisicao.valor = null;
					}
					else
					{
						debug('Possui valor %j', requestVal);
						parametroDaRequisicao.valor = parseFloat(requestVal);
					}
					//err = isNaN(requestVal);
				}
				else
				{
					debug('Sem valor para o parametro: %j', parametroDaRequisicao.nomeNaRequisicaoHttp);
					parametroDaRequisicao.valor = null;
				}
			break;
            case "datetime":
			case "date":
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.DateTime;
				debug("Parametro: %j, valor: %j",parametroDaRequisicao, requestVal );

				if(requestVal) // || parametroDaRequisicao.requerido)
				{
                    var dateInfo = parseDate(requestVal + '');
					debug("DATE: %j", dateInfo);
                    if(dateInfo.erro)
                        err = "Tipo de dado incompatível: é esperado um valor datetime no formato: 'YYYY-MM-dd hh:mm:ss'";
                    else if(dateInfo.data)
					{
						parametroDaRequisicao.valor = dateInfo.data;
					}
					else
					{
						parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.VarChar;
						parametroDaRequisicao.valor = '';
					}
				}
				else
                {
					debug("DATE: %j", 'VAZIO');
					parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.VarChar;
					parametroDaRequisicao.valor = '';
				}
			break;
			case "boolean":
			case "bool":
			case "boolean":
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Bit;
				var pValue = (requestVal + '').trim().toLocaleLowerCase();

				if(bValues.indexOf(pValue) == -1 )
				{
					err = "Tipo de dado incompatível: é esperado um valor booleano para o campo.";
					parametroDaRequisicao.valor = null;
				}
				else
				{
					//err = "Tipo de dado incompatível: é esperado um valor booleano para o campo.";
					parametroDaRequisicao.valor = eval(pValue);
				}
				//if(!parametroDaRequisicao.requerido && !requestVal)
				//{
				//	if(parametroDaRequisicao.valorDefault)
				//		parametroDaRequisicao.valor = eval('(' + parametroDaRequisicao.valorDefault + ')');
				//}
				//else if(parametroDaRequisicao.requerido && typeof(requestVal) !== 'boolean')
				//{
				//	err = true;
				//}
				//else{
				//	parametroDaRequisicao.valor = null;
				//}
			break;
			case "string":
			debug("Extracting string parameter: %j", parametroDaRequisicao);
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.VarChar;
				//if(!parametroDaRequisicao.requerido && !requestVal){
				//	if(parametroDaRequisicao.valorDefault)
				//		parametroDaRequisicao.valor = eval('(' + parametroDaRequisicao.valorDefault + ')');
				//}
				//else
				if(parametroDaRequisicao.requerido)
				{
					err = !requestVal;
					parametroDaRequisicao.valor = requestVal;
				}
				else{
					parametroDaRequisicao.valor = requestVal;
				}
				if(parametroDaRequisicao.valor)
					parametroDaRequisicao.valor = parametroDaRequisicao.valor.toString();
				debug("Valor da string %j %j", parametroDaRequisicao.valor, (parametroDaRequisicao.valor ? parametroDaRequisicao.valor.length : 0));
				debug("Tamanho maximo %j", parametroDaRequisicao.tamanhoNaRequisicao || 0);
				var temTamanhoMaximo = parametroDaRequisicao.tamanhoNaRequisicao || 0;
				if(temTamanhoMaximo && parametroDaRequisicao.valor && (parametroDaRequisicao.valor.length > temTamanhoMaximo)){
					parametroDaRequisicao.erro = "Valor do parâmetro " + parametroDaRequisicao.nomeNaRequisicaoHttp
						+ " excede o tamanho máximo. Tamanho máximo aceito:  " + temTamanhoMaximo + ' caracteres';
				}
			break;
			case "tvp":
				//debug("Extracting tvp parameter: %j, %j", parametroDaRequisicao, requestVal);
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.TVP;

				if(!(requestVal instanceof Array)){
					debug('Parâmetro incorreto: %j,  %j',parametroDaRequisicao,  requestVal);
					err = 'Parametro tabular inválido';

				}
				else
				{
                    var tipoSubParam = "";
					var arVal = [];

                    for(var kk=0; kk < requestVal.length; kk++){
						//var currRow = [(parametroDaRequisicao.ordemNaRequisicaoAoBanco + 1) * 100 +  kk + 1];
						var currRow = [kk + 1];
						//currRow.push(kk + 1);

						arVal.push(currRow);
                        var row = requestVal[kk];
						//debug(row);
						var keys = Object.keys(row);
						//debug(keys);
						//debug("Params: %j Keys length: %j", parametroDaRequisicao.parametros.length, keys.length);
                        if(keys.length != parametroDaRequisicao.parametros.length){
                            err = 'Quantidade de colunas incompatível com o requerido na linha ' + (kk + 1) + '. '
                            +    parametroDaRequisicao.parametros.length + ' parâmetros requeridos mas ' + keys.length + ' foram enviados.';

							break;
                        }
                        for(var jj=0; jj < parametroDaRequisicao.parametros.length; jj++)
						{


                            tipoSubParam = parametroDaRequisicao.parametros[jj].tipoNaRequisicaoAoBanco.toLowerCase();
							var fieldValue = row[parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp];

							debug("Parametro: %j, valor: %j", parametroDaRequisicao.parametros[jj], fieldValue);

                            if(tipoSubParam == "date" || tipoSubParam == "datetime")
							{

                                var dateInfo = parseDate(fieldValue + '');
                                if(dateInfo.erro)
								{
									debug('Date Error: %j', dateInfo );
                                    err = "Tipo de dado incompatível para o campo '"
										+ parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp +
										"' do item " + (kk + 1) + " da lista: É esperado um valor datetime no formato 'YYYY-MM-dd hh:mm:ss'.";
                                    break;
                                }
                                else
								{
									debug('Date: %j', dateInfo );
									row[parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp] = dateInfo.data || null;
                                }
                            }
							else if(tipoSubParam == "decimal")
							{
								if(fieldValue == "" || fieldValue == null)
								{
									row[parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp] = null;
								}
								else if(isNaN(fieldValue)){
									err = "Tipo de dado incompatível para o campo '"
										+ parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp +
										"' do item " + (kk + 1) + " da lista: É esperado um valor numérico para o campo.";
								}
								else
								{
									row[parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp] = parseFloat(fieldValue || '0');
								}
							}
							else if(tipoSubParam == "int")
							{
								var svalue = ('' + fieldValue);
								fieldValue = parseInt(fieldValue + '');
								debug('VAl: %j, %j', svalue, fieldValue);

								if(fieldValue == null || eval('(' + fieldValue + ')') == null || svalue == 'null')
								{
									row[parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp] = null;
								}
								else if(isNaN(fieldValue) || (fieldValue + '').indexOf('.') > -1 || fieldValue > 2147483647.0 || fieldValue < -2147483648.0 || svalue.length > 10 || svalue.indexOf('e+') != -1)
								{
									err = "Tipo de dado incompatível para o campo '"
									+ parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp +
									"' do item " + (kk + 1) + " da lista: É esperado um valor numérico inteiro para o campo.";
								}
								else
								{
									row[parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp] = fieldValue;
								}
							}
							else if(tipoSubParam == "string")
							{
								var temTamanhoMaximo = parametroDaRequisicao.parametros[jj].tamanhoNaRequisicao || 0;
								if(temTamanhoMaximo && fieldValue && ((fieldValue + '' ).length > temTamanhoMaximo))
								{
									parametroDaRequisicao.erro = "Valor do campo " + parametroDaRequisicao.parametros[jj].nomeNaRequisicaoHttp
										+ " da lista excede o tamanho máximo. Tamanho máximo aceito:  " + temTamanhoMaximo + ' caracteres';
									break;
								}
							}

							else if(tipoSubParam == "bool" || tipoSubParam == 'boolean') {
								//parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Bit;
								var pValue = (fieldValue + '').trim().toLocaleLowerCase();

								if (bValues.indexOf(pValue) == -1) {
									err = "Tipo de dado incompatível: é esperado um valor booleano para o campo.";
									parametroDaRequisicao.valor = null;
								}
								else {
									//err = "Tipo de dado incompatível: é esperado um valor booleano para o campo.";
									parametroDaRequisicao.valor = eval(fieldValue);
								}
							}
                        }

						for(var ki=0; ki < keys.length; ki++)
						{
							currRow.push(row[keys[ki]]);
						}

                    }

                    if(!err)
                    {
						var thecols = [{ name : "idtype", type : sqlServerDS.TYPES.Int }];
						thecols = thecols.concat(parametroDaRequisicao.parametros
							.map(p=>{ return { name : p.nomeNaRequisicaoHttp, type : getSubType(p) }; }));


                        var table = {
                            columns:thecols,
                            rows: arVal
                        };
                        parametroDaRequisicao.valor = table;
                    }
					else{
						debug(err);
					}
				}
			break;
			default:
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.VarChar;
			break;
		}
		if(err === true)
			parametroDaRequisicao.erro = "Valor inválido para parâmetro '" + parametroDaRequisicao.nomeNaRequisicaoHttp
			+ "'. " + parametroDaRequisicao.valor;
        else if(typeof (err) === 'string'){
            parametroDaRequisicao.erro = "Valor inválido para parâmetro '"
                + parametroDaRequisicao.nomeNaRequisicaoHttp + "'"
                + (parametroDaRequisicao.valor ? ( ": " + parametroDaRequisicao.valor + '. ') : '.') + err;
        }

};

function parseDate(text) {

    if(!text || text == 'null')
		return { erro : false, data : null };

	var parts 		= text.split(' ');
    var dateParts 	= parts[0].split('-');

    if(dateParts.length < 3) {

        return {  erro : true, data : null };
    }
    else
    {
        var ano = parseInt(dateParts[0]);
        var mes = parseInt(dateParts[1]) -1;
        var dia = parseInt(dateParts[2]);

        var hora = 0, minuto = 0, segundo = 0, ml = 0;

        if(parts.length == 1)
            return { erro : true, data : null };

        var timeParts = parts[1].split(':');

        if(timeParts.length > 1)
		{
            //var timeParts = dateParts[3].split(':');
            hora = parseInt(timeParts[0]);
            minuto = parseInt(timeParts[1]);
            console.log(1);

            if(timeParts.length == 3)
            //if(timeParts[2].indexOf('.') == -1)
			{
                console.log(2);
                segundo = parseInt(timeParts[2]);
			}
			else if(timeParts.length > 3)
			{
                console.log(3);
                var segParts = timeParts[2].split('.');
				segundo = parseInt(segParts[0]);
				ml = parseInt(segParts[1]);
			}
            console.log(4);
        }

        if(isNaN(ano) || isNaN(mes) || isNaN(dia) || isNaN(hora) || isNaN(minuto) || isNaN(segundo)|| (ml && isNaN(ml)))
		{
            return { erro : true };
        }
        else
        {
            return { erro: false , data : new Date(ano, mes, dia, hora, minuto, segundo, ml)};
        }
    }
}

module.exports.handleError = function(request, response) {

	return function(err) {

		debug(err);

		var message = err;
		var status  = 400;

		debug('Object Error %j', typeof(err));
		debug(message);

		if(typeof(err) == 'object') {

			if(err.status)
				status = err.status;

			message = err.message;
		}

		if((process.env.NODE_ENV == 'production' || process.env.NODE_ENV == 'hom' || !request.query.__debug) && err.severe) {

			message = "Não foi possível executar a operação. Verifique se todos os campos foram fornecidos e se seu tamanho e tipo de dado estão de acordo com o manual";
		}

		debug('sending error response');

		response.errorMessage = message + (err.logObs ? (' | ' + err.logObs) : '');
		response.status(status).send({
			erro: 'true',
			msg:  message
		});
	};
};