var debug       = require('debug')('orbe:config');
var path        = require('path');

var config = {
        port                    : 3048,
        redis                   : { host: "localhost", port: 6379 },
        myOrbeURL               : "http://192.168.0.90:3031/",
        //ftpFilesPath          : "/users/davisabino/desktop/files/",
        moduleId                : 38, //6, //6 //1,
        //managerModuleId       : 6,
        moduleName              : 'portal-resultados-claro',//'sgr',
        dbNameInMongo           : 'portal_resultado_claro_db',//'sgr_db',
        dataSources             : { },
        cacheTimeout            : 30,
        cacheRequestHit         : 20,
        logAPIURL               : "http://api.redeinova.com.br/log/",
        limiteRegistrosRequisicao : 100,
        pubSubChannel           : 'orbe-api-instance',
        getDataSource           : function(cliente) {
                return this.dataSources[cliente];
        },
        addDataSource           : function(cliente, obj) {
                debug("cliente: %j, val:%j", cliente, obj);
                this.dataSources[cliente] = obj;
        },
        removeDataSource        : function(cliente) {
                delete this.dataSources[cliente];
        },
        sgsServer: {
                userName: 'inovsgr',
                password: 'sgr@$#16',
                server:'10.177.51.32',
                options: {
                        instanceName: 'sgr',
                        database: 'sgs',
                        rowCollectionOnRequestCompletion: true,
                        requestTimeout: 30000
                }
        }
};

config.mongoURL = "mongodb://localhost:27017/" + config.dbNameInMongo;

if(process.env.NODE_ENV == 'hom') {

        config.port             = 3001;
        config.myOrbeURL        = 'http://localhost:3030/';

        //config.ftpFilesPath   = "/home/locus-ftp/";
        // config.dataSources[1] = {
        //      usuarioProprietario : 1,
        //      tipo : 'SQL SERVER',
        //      userName: 'sa',
        //      password: 'jm#srv@1!',
        //      server: 'srvinova',
        //      port: 1433,
        //      database: 'model_sgroutros'
        // };

}
else if (process.env.NODE_ENV == 'production') {

        config.mongoURL         = "mongodb://10.177.51.62:27017/" + config.dbNameInMongo;
        config.redis            = { host: "10.177.51.43", port: 6379 };
        config.myOrbeURL        = "http://10.177.51.43:80/my-orbe/";

        //config.mongoIp          = '10.177.51.62';
        //config.ftpFilesPath   = "/home/locus-ftp/";

        //config.mongoURL        = "mongodb://my-orbe:jmsrv1v2myorbe@" + config.mongoIp + ":" + config.mongoPort + "/" + config.dbInMongo;
        //config.redis            = { host: "10.177.51.43", port: 6379 };
}

debug('config: %j', config);

module.exports = config;
                                                                                                                                                      1,1           Top