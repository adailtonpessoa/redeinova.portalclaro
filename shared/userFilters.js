var debug = require('app-root-path').require('./shared/debug')('userFilters');

var filterAllUsers = function(users) {

	debug("filtering all users...");

	var formattedUsers = users.map(function(user) {

		return filterUser(user);
	});

	return formattedUsers;
};


var filterUser = function(user) {

	debug("filtering user...");

	return {
			id: user._id,
			name: user.name,
			orbeId: user.orbe_id,
			externalId: user.external_id
		};
};

exports.filterAllUsers 	= filterAllUsers;
exports.filterUser 		= filterUser;