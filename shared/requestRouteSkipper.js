var debug = require('app-root-path').require('./shared/debug')('requestRouteSkipper');

var routesToSkip = [
	{ route:'/css', method: 'get' },
	{ route:'/fonts', method: 'get' },
	{ route:'/images', method: 'get' },
	{ route: '/auth', method: 'post' },
];

function hasToSkip(request) {

	debug('checking if has to skip for route...');

	//check routes
	for(var i in routesToSkip) {

		var routeToSkip = routesToSkip[i];

		if(request.url.indexOf(routeToSkip.route) == 0) {

			if(routeToSkip.method == '*')
				return true;

			if(request.method.toLowerCase() == routeToSkip.method)
				return true;
		}
	}

	return false;
}

exports.hasToSkip = hasToSkip;