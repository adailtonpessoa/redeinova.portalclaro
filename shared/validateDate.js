var debug 	=  require('app-root-path').require('./shared/debug')('validateDate');
var Q 		= require('q');
var moment  = require('moment');

module.exports = function validateDate(date, hasToReturnBoolean) {

	debug('date: %j', date);

	if(!date)
		return hasToReturnBoolean ? false : Q.reject('Missing date');

	var momentDate = moment(date);

	if(momentDate.toString() == 'Invalid date' )
		return hasToReturnBoolean ? false : Q.reject('Invalid date');

	return hasToReturnBoolean ? true : Q( momentDate );
};