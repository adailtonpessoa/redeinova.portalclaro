var debug 		= require('app-root-path').require('./shared/debug')('validateId');
var Q 			= require('q');
var mongoose 	= require('mongoose');

var ObjectId = mongoose.Types.ObjectId;

module.exports = function validateId(id) {

	debug('doing...');

	if(typeof id != 'string')
		return Q.reject('The id must be a string');

	if(id.length == 0)
		return Q.reject('The id can not be empty');

	if(!ObjectId.isValid(id))
		return Q.reject('Invalid id');

	return Q();
};