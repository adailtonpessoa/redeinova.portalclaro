var debug = require('app-root-path').require('./shared/debug')('handleError');

module.exports = function handleError(response, error) {

	debug('handling error...');
	debug('sending error: ', error);

	if(typeof error == 'object' && !(error instanceof Array) && error.status) {

		var status  = error.status || 500;
		var message = error.message || 'Erro interno na API';

		response.errorMessage = message + (error.logObs ? (' | ' + error.logObs) : '');
		return response.status(status).send(message);
	}

	response.errorMessage =  'Erro interno na API | ' + message + (error.logObs ? (' | ' + error.logObs) : '');
	return response.status(500).send('Erro interno na API');
};