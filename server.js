var appRootPath = require('app-root-path');

var app 		= appRootPath.require('./app');
var redisDS 	= appRootPath.require('./data-sources/redisDS');
var ipFinder 	= appRootPath.require('./shared/ipFinder');
var config      = appRootPath.require('./shared/config');
var pubSub 		= appRootPath.require('./shared/pubSub');
var debug 		= appRootPath.require('./shared/debug')('server');

var version 	= '0.1.0';
var path 		= null;

function registerOnLoadBalancer() {

	debug('registering on load balancer...');

	// pubSub.publish({
	// 	operation: 'register',
	// 	path: path
	// });
};

function unregisterOnLoadBalancer() {

	debug('unregistering on load balancer...');

	pubSub.publish({
		operation: 'unregister',
		path: path
	});
};

function exit() {

	debug('exiting...');

	//unregisterOnLoadBalancer();
	process.exit(0);
}

//start server
var server = app.listen(process.env.PORT || config.port, function() {

	var addressInfo = server.address();
	var ips 		= ipFinder.find();

	console.log("Available ips: %j", ips);

	var selectedIp   = ips[0];

	console.log('Selected ip: %j', selectedIp.address);

	path = 'http://' + selectedIp.address + ':' + addressInfo.port;

	//registerOnLoadBalancer();

	//pubSub.subscribe('load-balancer-instance', 'started', registerOnLoadBalancer);

	console.log('Server listening on port: ' + addressInfo.port + '. Server version: ' + version + '.');

});

process.on('SIGINT', exit);

pubSub.subscribe('my-orbe-api-instance', 'restart', message => {

	var moduleId = message.moduleId;

	debug('restart message received...');
	debug('moduleId: %s', moduleId);
	debug('config.moduleId: %s', config.moduleId);

	if(moduleId == config.moduleId)
		exit();
});

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';