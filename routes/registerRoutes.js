var express 	= require('express');
var arp 		= require('app-root-path');
var q 			= require('q');

var debug = arp.require('./shared/debug')('registerRoutes');

//resourcer
var router = express.Router();

//Business Logic

var myOrbeDS				= arp.require('./data-sources/myOrbeDS');
//var redisClient			= arp.require('./data-sources/redisDS');
var getHandler				= arp.require('./routes/getHandler');
var postHandler				= arp.require('./routes/postHandler');
var putHandler				= arp.require('./routes/putHandler');
var deleteHandler			= arp.require('./routes/deleteHandler');
var CustomresourcerCache	= arp.require('./routes/customRouter');
var config 					= arp.require('./shared/config');
var UserShare               = arp.require('./models/userShare');
//debug('Getting user share: ' + UserShare);

function saveDataSources(dataSources){
    //debug('Saving databases %j', dataSources);
	return q.all(dataSources.map(function(ds){
        return saveDataSource(ds);
    }));
}

function saveDataSource(dataSource){
    //debug('Saving 0: %j', dataSource);
    config.addDataSource(dataSource.idUsuarioProprietario, dataSource);
}

function saveUserShareList(shareList){
   // debug('Saving databases %j', shareList);

    return q.all(shareList.map(function(share){
        return saveUserShare(share);
    }));
}

function saveUserShare(share){
    //debug("savingUserShare %j", share);
    return q.all(share.recursos.map(function(recurso){
        return saveUserResource({
            idProprietario : share.idProprietario,
            nome : recurso.nome,
            limite : recurso.limite
		});
    }));
}

function saveUserResource(share){
	return getUserShareFromDB(share)
        .then(checkExists);
}

function getUserShareFromDB(share){

    var date = new Date();

    date = new Date(date.getFullYear(), date.getMonth(), date.getDate());

    //debug('Getting from DB %j', share);

    var deferred = q.defer();

    UserShare.findOne({
        idProprietario :  share.idProprietario,
        vigencia : date,
        recurso : share.nome
    },function(err, userShare){
        debug('found : %j', userShare);
        if(!userShare)
        {
            userShare = {
                idProprietario :share.idProprietario,
                recurso : share.nome,
                limite : {  quantidadeRequisicoes : share.limite, dataAtualizacao : new Date() }
            };
        }
        else
        {
            debug("UserShare x share: %j x %j", userShare, share);
            if(userShare.limite.quantidadeRequisicoes != share.limite)
            {
                userShare.limite.quantidadeRequisicoes = share.limite;
                userShare.limite.dataAtualizacao = new Date();
                userShare.ativo = true;
                debug('updating limite');
            }
        }
        //debug("updating %j", userShare);
        deferred.resolve(userShare);
    });

    return deferred.promise;
}

function checkExists(userShare){
    //debug('Checking existence %j', userShare);
    if(!userShare._id)
    {
        debug('Creating new UserShare');
        var date = new Date();
        date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
        userShare = new UserShare(userShare);
        userShare.vigencia = date;
        userShare.quantidadeRequisicoes = 0;
        userShare.dataEmQueExcedeu = null;
        userShare.foiExcedido = false;
        userShare.ativo = true;

        debug('Ending creating new UserShare');
    }


    return userShare.save();
}

function saveUserShareInDB(opts){
    //debug('Saving %j', opts);
    return opts.share.save();
}


var Customresourcer = new CustomresourcerCache();

//debug("Custom resourcer %j", CustomresourcerCache);

function buildresources(resources){

	//debug("Building resources: %j", resources);

	for (var i = 0; i < resources.length; i++) {
		var resource = resources[i];
		var customresource = {};
		debug("resource %j", i);

		Customresourcer.addResource(resource.nome);

		debug("building endpoints");

		for (var j = 0; j < resource.operacoes.length; j++) {
			var rota = resource.operacoes[j];

			buildEndpoint(resource, rota);

			//debug("Endpoint: %j added", rota);
			Customresourcer.addRoute(resource.nome, rota);
			//debug("Added to resourcer: %j", rota.metodo);
		}
	}
	debug("Finishing building resources");
	return q();
}

function buildEndpoint(resource, rota){
	if(rota.metodo.toUpperCase() == "GET"){
		debug("Registering resource %j ", resource.nome);

		router.get('/' + resource.nome, function(req, res){
			debug('Resolving: ' + resource.nome);
			var resolvedEndpoint = Customresourcer.resolveRequest(resource.nome, "GET", req.query || { }, req.license.usuarioProprietario.id); //req.client.id);
			if(resolvedEndpoint.resolved)
				return getHandler(resolvedEndpoint.rota, req, res);
			debug('Não encontrado');
			res.status(404).send(resolvedEndpoint.message);
		});
	}
	else if(rota.metodo.toUpperCase() == "POST"){
		router.post('/' + resource.nome, function(req, res){
			debug('Post to ' + resource.nome); 
			var resolvedEndpoint = Customresourcer.resolveRequest(resource.nome, "POST", req.body, req.license.usuarioProprietario.id);  //req.client.id);
			if(resolvedEndpoint.resolved)
				return postHandler(resolvedEndpoint.rota, req, res);

			res.status(404).send(resolvedEndpoint.message);
		});
	}
	else if(rota.metodo.toUpperCase() == "PUT"){
		router.put('/' + resource.nome, function(req, res){
			var resolvedEndpoint = Customresourcer.resolveRequest(resource.nome, "PUT", req.body || {}, req.license.usuarioProprietario.id); //request.client.id);
			if(resolvedEndpoint.resolved)
				return putHandler(resolvedEndpoint.rota, req, res);

			res.status(404).send(resolvedEndpoint.message);
		});
	}
	else if(rota.metodo.toUpperCase() == "DELETE"){
		router.delete('/' + resource.nome, function(req, res){

			if(req.body)
				for (var attrname in req.body)
					req.query[attrname] = req.body[attrname];
            debug('Delete');
            debug(req.query);
			var resolvedEndpoint = Customresourcer.resolveRequest(resource.nome, "DELETE", req.query || {}, req.license.usuarioProprietario.id); //request.client.id);
			if(resolvedEndpoint.resolved)
				return deleteHandler(resolvedEndpoint.rota, req, res);

			res.status(404).send(resolvedEndpoint.message);
		});
	}
	else{
		res.status(404).send('Não encontrado');
	}
}


debug('Loading endpoints');

q.delay(1500)
.then(myOrbeDS.listDynamicResourcesForModule)
.then(buildresources)
.then(myOrbeDS.listDataSources)
.then(saveDataSources)
.then(myOrbeDS.listUserShare)
.then(saveUserShareList)
.catch(function(err){
	debug("Erro: %j", err);
})
.done();

module.exports = router;