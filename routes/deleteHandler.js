var arp 			= require('app-root-path');
var q				= require('q');
var sqlServerDS		= arp.require('./data-sources/sqlServerDS');
var debug 			= arp.require('./shared/debug')('deleteHandler');
var utils			= arp.require('./shared/utils');

var _handleError 	= arp.require('./shared/handleError');

module.exports = function(endpoint, request, response){

	function validateRequest(){
		debug('Validar...');
		debug(endpoint.parametrosDaRequisicao);

		for (var i = 0; i < endpoint.parametrosDaRequisicao.length; i++) {
			var parametroDaRequisicao = endpoint.parametrosDaRequisicao[i];
			utils.validateType(parametroDaRequisicao, request.query);
			if(parametroDaRequisicao.erro)
				return q.reject( { status : 400, message : parametroDaRequisicao.erro });
		};
		return q();
	}

	function getParameters(parametrosDaRequisicao){
		debug("Getting parameters from request: %j", parametrosDaRequisicao);
		var parameters = [];
		for (var i = 0; i < parametrosDaRequisicao.length; i++) {
			var parameterBanco = parametrosDaRequisicao[i];
			debug("Parameter from request: %j", parameterBanco.valor);
			var novoParametro = {
				name : parameterBanco.nomeNaRequisicaoAoBanco || parameterBanco.nomeNaRequisicaoHttp,
				type : parameterBanco.tipoBanco,
				value : parameterBanco.valor,
			};

			if(parameterBanco.tipoBanco.name == sqlServerDS.TYPES.Decimal.name){
				novoParametro.scale = 5;
				novoParametro.precision = 15;
			}

			if(parameterBanco.tipoBanco.name == sqlServerDS.TYPES.TVP.name){
				for(var k = 0; k < parameterBanco.valor.columns.length; k++){
					if(parameterBanco.valor.columns[k].type.name == sqlServerDS.TYPES.Decimal.name){
						parameterBanco.valor.columns[k].scale = 5;
						parameterBanco.valor.columns[k].precision = 15;
					}
				}
			}
			debug("Parameter from request: %j", parameterBanco.valor);
			parameters.push(novoParametro);
		};
		return parameters;

	}

	function execOperation(){

		if(true || endpoint.recursoDoBanco.tipo == "Procedure"){

			var parameters = getParameters(endpoint.parametrosDaRequisicao);

			return sqlServerDS.callProcedure(endpoint.recursoDoBanco.nome, parameters, request.license.usuarioProprietario.id);
		}
	}

	function filterResponse(res) {

		if(typeof res == 'string') {

			if(res.indexOf('The DELETE statement conflicted with the REFERENCE constraint'))
				return "Erro ao executar a operação";
		}

		return res;
	}

	function sendResponse(res) {

		response.status(200).send(res);
	}

	validateRequest()
	.then(execOperation)
	.then(sendResponse)
	.catch(utils.handleError(request, response));

};
