var arp 	= require('app-root-path');
var debug 	= arp.require('./shared/debug')('CustomRouter');

module.exports = function(){
	var data = {};
	this.addResource = function(resourceName){
		//debug('adding route %j', resourceName);

		var resource = {
			endpoints : []
		};
		data[resourceName] = resource;
	};

	this.getResource = function(resourceName){
		debug('get route %j', resourceName);
		return data[resourceName];
	};

	this.addRoute = function(resourceName, rota){
		var resource = data[resourceName];
		var resourceEndpoint = {
			params : [],
			rota : rota,
			custom : {}
		};

		debug("Adding rota %j for resource %j", rota, resource);

		rota.metodo = rota.metodo.toUpperCase();
		if(rota.parametrosDaRequisicao)
			for (var i = 0; i < rota.parametrosDaRequisicao.length; i++) 
			{
				resourceEndpoint.params.push((rota.parametrosDaRequisicao[i].nomeNaRequisicaoHttp + '').toLowerCase()); 
			}

		//debug("Parameters for new rota: %j", resourceEndpoint.params);
		//
		//debug("Finishing adding rota: %j", resourceEndpoint);

		resource.endpoints.push(resourceEndpoint);
 	};

 	this.resolveRequest = function(resourceName, method, queryParams, clientId){

 		var resource = data[resourceName];
 		if(!resource)
 			return {
 				resolved : false,
 				message : "Rota não encontrada"
 			};
		debug("resource: %j", resource);
 		var selectedEndpoints = [];

 		debug("Resolving request: %j", resource);

 		for (var i = 0; i < resource.endpoints.length; i++) {
 			var resourceEndpoint = resource.endpoints[i];

 			debug("Checking rota: %j, for method: %j", resourceEndpoint, method);

 			if(resourceEndpoint.rota.metodo != method)
 				continue;
			var usuariosPermitidos = resourceEndpoint.rota.usuariosPermitidos;
			if(usuariosPermitidos && usuariosPermitidos.length > 0){
				for( var j = 0; j < usuariosPermitidos.length; j++)
				{
					if(usuariosPermitidos[j].idProprietario == clientId){
						selectedEndpoints.push(resourceEndpoint.rota);
					}
				}
			}
			else{
				selectedEndpoints.push(resourceEndpoint.rota);
			}

 		}

 		debug('Selected endpoints: \n%j', selectedEndpoints);

 		if(selectedEndpoints.length == 0){
 			return {
 				resolved : false,
 				message : "Rota não encontrada"
 			};
 		}
 		debug('Selecionando versão de endpoint %j', queryParams);
 		if(queryParams.__v)
 		{
 			for (var i = 0; i < selectedEndpoints.length; i++) {
 				if(selectedEndpoints[i].versao == queryParams.__v)
				{

					return {
				 		resolved : true,
						rota : cloneRoute(selectedEndpoints[i])
 					};
 				}
 			}

 			return {
 				resolved : false,
 				message : "Versão inválida"
 			};
 		}
 		else
	 		for (var i = 0; i < selectedEndpoints.length; i++)
			{
				if(selectedEndpoints[i].padrao) {

					return {resolved: true, rota: cloneRoute(selectedEndpoints[i]) };

				}
			}

		return {
			resolved : true,
			rota : cloneRoute(selectedEndpoints[selectedEndpoints.length -1])
		};

 	};

	function cloneRoute(route){


		var selectedCopy = {
			metodo : route.metodo,
			recursoDoBanco : route.recursoDoBanco,
			parametrosDaRequisicao : []
		};

		for (var i = 0; i < route.parametrosDaRequisicao.length; i++)
		{
			selectedCopy.parametrosDaRequisicao.push(cloneParams(route.parametrosDaRequisicao[i]));
		}

		return selectedCopy;
	}

	function cloneParams(param){
		var paramCopy = {
			nomeNaRequisicaoAoBanco : param.nomeNaRequisicaoAoBanco,
			nomeNaRequisicaoHttp : param.nomeNaRequisicaoHttp,
			ordemNaRequisicaoAoBanco : param.ordemNaRequisicaoAoBanco,
			tamanhoNaRequisicao : param.tamanhoNaRequisicao,
			parametros : [],
			tipoNaRequisicaoAoBanco : param.tipoNaRequisicaoAoBanco
		};

		debug(param);
		if(param.tipoNaRequisicaoAoBanco == 'TVP'){
			for(var i = 0; i < param.parametros.length; i++){
				paramCopy.parametros.push(cloneParams(param.parametros[i]));
			}
		}

		return paramCopy;
	}
};