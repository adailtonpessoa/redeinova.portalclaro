var arp 			= require('app-root-path');
var q				= require('q');

var sqlServerDS		= arp.require('./data-sources/sqlServerDS');
var debug 			= arp.require('./shared/debug')('putHandler');

var _handleError 	= arp.require('./shared/handleError');
var utils			= arp.require('./shared/utils');


module.exports = function(endpoint, request, response){

	function validateType(parametroDaRequisicao){
		var requestVal = request.body[parametroDaRequisicao.nomeNaRequisicaoHttp];
		debug("Valor do parametro %j: %j", parametroDaRequisicao.nomeNaRequisicaoHttp, requestVal);
		var err = false;
		switch(parametroDaRequisicao.tipoNaRequisicaoAoBanco.toLowerCase()){
			case "integer":
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Int;

				if(!parametroDaRequisicao.requerido && !requestVal){
					if(parametroDaRequisicao.valorDefault)
						parametroDaRequisicao.valor = eval('(' + parametroDaRequisicao.valorDefault + ')');
				}
				else if(requestVal || parametroDaRequisicao.requerido)
				{
					parametroDaRequisicao.valor = parseInt(requestVal);
					err = isNaN(parametroDaRequisicao.valor);
				}

			break;
			case "decimal":
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Decimal;
				if(!parametroDaRequisicao.requerido && !requestVal){
					if(parametroDaRequisicao.valorDefault)
						parametroDaRequisicao.valor = eval('(' + parametroDaRequisicao.valorDefault + ')');
				}
				else if(requestVal || parametroDaRequisicao.requerido)
				{
					parametroDaRequisicao.valor = parseFloat(requestVal);
					err = isNaN(parametroDaRequisicao.valor);
				}
			break;
			case "date":
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.DateTime;
				if(!parametroDaRequisicao.requerido && !requestVal){
					if(parametroDaRequisicao.valorDefault)
						parametroDaRequisicao.valor = eval('(' + parametroDaRequisicao.valorDefault + ')');
				}
				else if(requestVal || parametroDaRequisicao.requerido)
				{
					var dateParts = requestVal.split('-');
					if(dateParts.length != 3)
						err = true;
					else
					{
						var ano = parseInt(dateParts[0]);
						var mes = parseInt(dateParts[1]);
						var dia = parseInt(dateParts[2]);
						if(isNaN(ano) || isNaN(mes) || isNaN(dia))
							err = true;
						else
						{
							parametroDaRequisicao.valor = new Date(ano, mes, dia);
						}
					}
				}
			case "boolean":
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.Bit;
				if(!parametroDaRequisicao.requerido && !requestVal){
					if(parametroDaRequisicao.valorDefault)
						parametroDaRequisicao.valor = eval('(' + parametroDaRequisicao.valorDefault + ')');
				}
				else if(requestVal || parametroDaRequisicao.requerido)
				{
					var dateParts = requestVal.split('-');
					if(dateParts.length != 3)
						err = true;
					else
					{
						var ano = parseInt(dateParts[0]);
						var mes = parseInt(dateParts[1]);
						var dia = parseInt(dateParts[2]);
						if(!isNaN(ano) || !isNaN(mes) || !isNaN(dia))
							err = true;
						else
						{
							parametroDaRequisicao.valor = new Date(ano, mes, dia);
						}
					}
				}
			break;
			case "string":
			debug("Extracting string parameter: %j", parametroDaRequisicao);
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.VarChar;
				if(!parametroDaRequisicao.requerido && !requestVal){
					if(parametroDaRequisicao.valorDefault)
						parametroDaRequisicao.valor = eval('(' + parametroDaRequisicao.valorDefault + ')');
				}
				else if(parametroDaRequisicao.requerido)
				{
					err = !requestVal;
					parametroDaRequisicao.valor = requestVal;
				}
				else{
					parametroDaRequisicao.valor = requestVal;
				}

			break;
			default:
				parametroDaRequisicao.tipoBanco = sqlServerDS.TYPES.VarChar;
			break;
		}
		if(err)
			parametroDaRequisicao.erro = "Valor inválido para parâmetro " + parametroDaRequisicao.nomeNaRequisicaoHttp;
	}

	function validateRequest(){

		for (var i = 0; i < endpoint.parametrosDaRequisicao.length; i++) {
			var parametroDaRequisicao = endpoint.parametrosDaRequisicao[i];
			utils.validateType(parametroDaRequisicao, request.body);
			if(parametroDaRequisicao.erro)
				return q.reject( { status : 400, message : parametroDaRequisicao.erro });
		};
		return q();
	}


/*
	function getParameters(parametrosDaRequisicao){

		var parameters = [];
		for (var i = 0; i < parametrosDaRequisicao.length; i++) {
			var parameterBanco = parametrosDaRequisicao[i];
			var novoParametro = {
				name : parameterBanco.nomeNaRequisicaoAoBanco || parameterBanco.nomeNaRequisicaoHttp,
				type : parameterBanco.tipoBanco,
				value : parameterBanco.valor
			};
			if(novoParametro.type.name == sqlServerDS.TYPES.Decimal.name)
				novoParametro.options = { precision : 18, scale : 8 };
			parameters.push(novoParametro);
		};
		return parameters;

	}
*/

	function getParameters(parametrosDaRequisicao){
		debug("Getting parameters from request: %j", parametrosDaRequisicao);
		var parameters = [];
		for (var i = 0; i < parametrosDaRequisicao.length; i++) {
			var parameterBanco = parametrosDaRequisicao[i];
			debug("Parameter from request: %j", parameterBanco.valor);
			var novoParametro = {
				name : parameterBanco.nomeNaRequisicaoAoBanco || parameterBanco.nomeNaRequisicaoHttp,
				type : parameterBanco.tipoBanco,
				value : parameterBanco.valor,
			};

			if(parameterBanco.tipoBanco.name == sqlServerDS.TYPES.Decimal.name){
				novoParametro.options = { scale : 8,
					precision : 18
				}
			}

			var novoParametro = {
				name : parameterBanco.nomeNaRequisicaoAoBanco || parameterBanco.nomeNaRequisicaoHttp,
				type : parameterBanco.tipoBanco,
				value : parameterBanco.valor
			};


			if(parameterBanco.tipoBanco.name == sqlServerDS.TYPES.TVP.name){
				for(var k = 0; k < parameterBanco.valor.columns.length; k++){
					if(parameterBanco.valor.columns[k].type.name == sqlServerDS.TYPES.Money.name){
						debug("Decimal da tabela: %j", parameterBanco.valor.columns[k]);

						//parameterBanco.valor.columns[k].scale = 8;
						//parameterBanco.valor.columns[k].precision = 18;
						//};
						//parameterBanco.valor.columns[k].precision = 15;
					}
				}
			}
			//debug("Parameter from request: %j", parameterBanco.valor);
			parameters.push(novoParametro);
		};
		return parameters;

	}

	function generateProcSQL(nomeProc, parameters){
		return "exec " + nomeProc + ' ' + parameters.map(function(p){ return '@' + p.name; }).join();
	}

	function execOperation(){

		if(true || endpoint.recursoDoBanco.tipo == "procedure"){
			var parameters = getParameters(endpoint.parametrosDaRequisicao);
			//var sql = generateProcSQL(endpoint.recursoDoBanco.nome, parameters);
			//debug("Parameters: %j", parameters);
			//debug("SQL: %j", sql);
			return sqlServerDS.callProcedure(endpoint.recursoDoBanco.nome, parameters, request.license.usuarioProprietario.id);
			//return sqlServerDS.sendCommand(sql, parameters, request.license.usuarioProprietario.id);
		}
	}

	function filterResult(data){

		return q(data);
	}

	function sendResponse(filteredData){

		debug("Data: %j", filteredData);

		response.status(200).send(filteredData);
	}

	validateRequest()
	.then(execOperation)
	//.then(filterResult)
	.then(sendResponse)
	.catch(utils.handleError(request, response));

};
