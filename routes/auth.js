var express 	= require('express');
var arp 		= require('app-root-path');
var q 			= require('q');
var debug 		= arp.require('./shared/debug')('auth');
var auth 		= arp.require('./business-logic/auth'); 

var router = express.Router();

router.post('/',auth);

module.exports = router;

