/**
 * Created by davisabino on 28/04/16.
 */

var express 	    = require('express');
var arp 		    = require('app-root-path');
var q 		    	= require('q');
var debug 		    = arp.require('./shared/debug')('reports');
var getReports 		= arp.require('./business-logic/reports/getReports');
var searchTerm 	    = arp.require('./business-logic/reports/search');
var listValues 	    = arp.require('./business-logic/reports/listValues');
var getData 	    = arp.require('./business-logic/reports/data');

var router = express.Router();

router.get('/',getReports);
router.get('/list-values/:id_report/param/:id_param', listValues);
router.get('/search/:id_report/param/:id_param/term/:term', searchTerm);
router.post('/data/:id_report', getData);

module.exports = router;

