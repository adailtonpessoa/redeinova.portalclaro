var arp 			= require('app-root-path');
var q				= require('q');

var sqlServerDS		= arp.require('./data-sources/sqlServerDS');
var debug 			= arp.require('./shared/debug')('getHandler');

var _handleError 	= arp.require('./shared/handleError');
var utils 			= arp.require('./shared/utils');
var config			= arp.require('./shared/config');


module.exports = function(endpoint, request, response){


	function validateRequest(){

		for (var i = 0; i < endpoint.parametrosDaRequisicao.length; i++) {
			var parametroDaRequisicao = endpoint.parametrosDaRequisicao[i];
			utils.validateType(parametroDaRequisicao, request.query);
			if(parametroDaRequisicao.erro)
				return q.reject( { status : 400, message : parametroDaRequisicao.erro });
		}
		return q();
	}

	function getParameters(parametrosDaRequisicao){
		debug("Getting parameters from request: %j", parametrosDaRequisicao);
		var parameters = [];
		for (var i = 0; i < parametrosDaRequisicao.length; i++) {
			if(parametrosDaRequisicao[i].nomeNaRequisicaoAoBanco == "sequencial")
				continue;
			var parameterBanco = parametrosDaRequisicao[i];
			var novoParametro = {
				name : parameterBanco.nomeNaRequisicaoAoBanco || parameterBanco.nomeNaRequisicaoHttp,
				type : parameterBanco.tipoBanco,
				value : parameterBanco.valor
			};
			if(novoParametro.type.name == sqlServerDS.TYPES.Decimal.name || novoParametro.type.name == sqlServerDS.TYPES.Money.name){
				novoParametro.options = { precision : 18, scale : 8 };
				novoParametro.precision = 18;
				novoParametro.scale = 8;
			}
			parameters.push(novoParametro);
		}

		//var sequencial = null;

		if(request.query.sequencial) {

			var sequencial = parseInt(request.query.sequencial);
			sequencial = !isNaN(sequencial) ? sequencial : 0;

			//if(!isNaN(sequencial)) {

				parameters.push({
					name : 'sequencial',
					type : sqlServerDS.TYPES.Int,
					value : sequencial
				});
			//}
		}
		else{
			parameters.push({
				name : 'sequencial',
				type : sqlServerDS.TYPES.Int,
				value : 0
			});
		}

		var limite = config.limiteRegistrosRequisicao || 100;
		//debug("Limite: %j", limite);
		if(request.query.limite) {

			var reqLimite = parseInt(request.query.limite);
			limite = !isNaN(reqLimite) ? reqLimite : limite;
		}

		parameters.push({
			name : 'limite',
			type : sqlServerDS.TYPES.Int,
			value : limite
		});

		return parameters;

	}

	function generateProcSQL(nomeProc, parameters){
		return "exec " + nomeProc + ' ' + parameters.map(function(p){ return '@' + p.name; }).join();
	}

	function retrieveData(){
		//var deferred = q.defer();
		if(true || endpoint.recursoDoBanco.tipo == "procedure"){
			var parameters = getParameters(endpoint.parametrosDaRequisicao);
			//var sql = generateProcSQL(endpoint.recursoDoBanco.nome, parameters);
			//debug("Parameters: %j", parameters);
			//debug("SQL: %j", sql);
			//debug("Licenca: %j", request.license);
			//return sqlServerDS.sendCommand(sql, parameters, request.license.usuarioProprietario.id);
			return sqlServerDS.callProcedure(endpoint.recursoDoBanco.nome, parameters, request.license.usuarioProprietario.id);

		}
	}

	function filterResult(data){

		if(endpoint.recursoDoBanco.mapeamentos && endpoint.recursoDoBanco.mapeamentos.length > 0)
		{
			/*
				nomeNaRespostaDoBanco: String,
				nomeNaRespostaHttp: String,
	 			tipoNaRespostaHttp: String
			*/

			debug('Mapeamentos.: %j', endpoint.recursoDoBanco.mapeamentos);
			var filteredResult = [];
			for (var i = 0; i < data.length; i++) {
				var filteredData = {};
				for (var k = 0; k < endpoint.recursoDoBanco.mapeamentos.length; k++) {
					filteredData[endpoint.recursoDoBanco.mapeamentos[k].nomeNaRespostaHttp]
						= data[i][endpoint.recursoDoBanco.mapeamentos[k].nomeNaRespostaDoBanco];

				}
				filteredResult.push(filteredData);
			}
			return q(filteredResult);
		}
		else
		{
			return q(data);
		}
	}

	function sendResponse(filteredData){
		response.status(200).send(filteredData);
	}


	validateRequest()
	.then(retrieveData)
	.then(filterResult)
	.then(sendResponse)
	.catch(utils.handleError(request, response));

};
