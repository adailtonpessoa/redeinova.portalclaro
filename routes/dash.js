/**
 * Created by davisabino on 28/04/16.
 */

var express 	    = require('express');
var arp 		    = require('app-root-path');
var q 		    	= require('q');
var debug 		    = arp.require('./shared/debug')('reports');
var getDashboard 	    = arp.require('./business-logic/resoluto/getDashboard');

var router = express.Router();

router.get('/', getDashboard);

module.exports = router;

