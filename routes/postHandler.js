var arp 			= require('app-root-path');
var q				= require('q');

var sqlServerDS		= arp.require('./data-sources/sqlServerDS');
var debug 			= arp.require('./shared/debug')('postHandler');

var _handleError 	= arp.require('./shared/handleError');
var utils			= arp.require('./shared/utils');


module.exports = function(endpoint, request, response){

	function validateRequest(){

		for (var i = 0; i < endpoint.parametrosDaRequisicao.length; i++) {
			var parametroDaRequisicao = endpoint.parametrosDaRequisicao[i];

			utils.validateType(parametroDaRequisicao, request.body);

			if(parametroDaRequisicao.erro)
				return q.reject( { status : 400, message : parametroDaRequisicao.erro });
		};
		return q();
	}

	function getParameters(parametrosDaRequisicao){
		//debug("Getting parameters from request: %j", parametrosDaRequisicao);
		var parameters = [];
		for (var i = 0; i < parametrosDaRequisicao.length; i++) {
			var parameterBanco = parametrosDaRequisicao[i];
			//debug("Parameter from request: %j", parameterBanco.valor);
			var novoParametro = {
				name : parameterBanco.nomeNaRequisicaoAoBanco || parameterBanco.nomeNaRequisicaoHttp,
				type : parameterBanco.tipoBanco,
				value : parameterBanco.valor,
			};

			if(parameterBanco.tipoBanco.name == sqlServerDS.TYPES.Decimal.name){
				novoParametro.options = { scale : 8,
					precision : 18
				}
			}

			if(parameterBanco.tipoBanco.name == sqlServerDS.TYPES.TVP.name){
				for(var k = 0; k < parameterBanco.valor.columns.length; k++){
					if(parameterBanco.valor.columns[k].type.name == sqlServerDS.TYPES.Decimal.name){
						parameterBanco.valor.columns[k].options = { scale : 8,
							precision : 18
						};
						//parameterBanco.valor.columns[k].precision = 15;
					}
				}
			}
			//debug("Parameter from request: %j", parameterBanco.valor);
			parameters.push(novoParametro);
		};
		return parameters;

	}

	function generateProcSQL(nomeProc, parameters){
		return "exec " + nomeProc + ' ' + parameters.map(function(p){ return '@' + p.name; }).join();
	}

	function execOperation(){
		
		if(true || endpoint.recursoDoBanco.tipo == "procedure"){
			var parameters = getParameters(endpoint.parametrosDaRequisicao);
			//var sql = generateProcSQL(endpoint.recursoDoBanco.nome, parameters);
			return sqlServerDS.callProcedure(endpoint.recursoDoBanco.nome, parameters, request.license.usuarioProprietario.id);
		}
		return q();
	}

	function filterResult(data){
		return q(data);
	}

	function sendResponse(filteredData){
		response.status(200).send(filteredData);
	}


	//function handleError(error){
	//	debug('handling error...');
	//	debug('sending error: ', error);
    //
	//	if(typeof error == 'object' && !(error instanceof Array) && error.status) {
    //
	//		var status  = error.status || 500;
	//		var message = error.message || 'Erro interno na API';
    //
	//		//response.errorMessage = message;
	//		return response.status(status).send(message);
	//	}
    //
	//	//response.errorMessage = 'Erro interno na API';
	//	return response.status(500).send('Erro interno na API');
	//}

	validateRequest()
	.then(execOperation)
	.then(filterResult)
	.then(sendResponse)
	.catch(utils.handleError(request, response));
	

};
