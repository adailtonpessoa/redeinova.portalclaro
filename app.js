//Modules
var express 	= require('express');
var bodyParser 	= require('body-parser');
var assert 		= require('assert');
var appRootPath = require('app-root-path');
var cors		= require('cors');
var debug		= appRootPath.require('./shared/debug')('app');
var morgan 		= require('morgan');

var handleError = appRootPath.require('./shared/handleError');

//Data Sources
var mongoDS	= appRootPath.require('./data-sources/mongoDS');

//Routes
var registerRoutes 	= appRootPath.require('./routes/registerRoutes');
var authRoute		= appRootPath.require('./routes/auth');
var reportsRoute	= appRootPath.require('./routes/reports');
var dashRoute	    = appRootPath.require('./routes/dash');
console.log('Dash route ' + dashRoute);
//Custom middlewares
var tracker						= appRootPath.require('./middlewares/tracker');
var signaturePayloadExtractor 	= appRootPath.require('./middlewares/signaturePayloadExtractor');
var licenseChecker 				= appRootPath.require('./middlewares/licenseChecker');
var requestShare 				= appRootPath.require('./middlewares/requestShare');


//Middlewares
var app = express();

app.use(tracker);
app.use(cors());
app.use(morgan('combined'));
app.use(bodyParser.json({ limit: '1mb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + "/public"));
app.use(signaturePayloadExtractor);
app.use(licenseChecker);
//app.use(requestShare);

//Route Path
app.use('/api', registerRoutes);
app.use('/auth', authRoute);
app.use('/reports', reportsRoute);
app.use('/ticket', dashRoute);

app.use((request, response) => handleError(response, { status: 404, message: 'Não encontrado' }));

mongoDS.connect();

module.exports = app;
