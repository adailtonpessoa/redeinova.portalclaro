var cluster 	= require('cluster');

if (cluster.isMaster) {

    // Count the machine's CPUs
	var cpuCount = require('os').cpus().length;

	console.log(cpuCount + " cpu's available...");

	// Create a worker for each CPU
	for (var i = 0; i < cpuCount; i++)
		cluster.fork();

    cluster.on('exit', function(worker) {

		console.log('Worker %d died :(', worker.id);
		console.log('Starting another one...');
		cluster.fork();
    });
}
else {

	// Code to run if we're in a worker process
	require('./server');
}