/*ATENÇÃO: colocar dependência no módulo*/
angular.module('orbe.directives')
    .directive('focusVoltar', function() {
        var ddo = {};
        ddo.restrict = 'A'; //(A)ttribute, (E)lement
        ddo.link = function(scope, element) {
            scope.$on("crudVoltar",function() {  
                element[0].focus();
            });
        };
        
        return ddo;
});