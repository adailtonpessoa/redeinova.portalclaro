angular.module('orbe.directives')
    .directive('selectRegionais', function () {
        var ddo = {};
        ddo.restrict = 'E';
        ddo.scope = {
            name: '@name',
            model: '=ngModel',
            change: '&'
        };
        //ddo.template = "<select name=\"{{name}}\" class=\"form-control\" ng-model=\"model\" required ng-options=\"r.Id as r.Nome for r in regionais\"></select>";
        ddo.template = "<md-select name=\"{{name}}\" ng-change=\"change()\" required ng-model=\"model\" aria-label=\"Regional\"><md-option ng-repeat=\"reg in regionais\" ng-value=\"reg.Id\">{{reg.Nome}}</md-option></md-select>";
        ddo.controller = function ($scope, WebAPI) {
            $scope.regionais = [];
            WebAPI.getEntidadeApi("regionais")
                .then(function (dados) {
                    $scope.regionais = dados.data;
                })                
                .catch(function (erro) {
                    console.log(erro);
                });
        }
        return ddo;
    })
    .directive("selectDdds", function () {
        return {
            restrict: "E",
            scope: {
                name: "@",
                model: "=ngModel",
                regionalField: '=regional',
                change: '&'
            },
            controller: function ($scope, DDDService) {
                $scope.ddds = [];
                DDDService.listar()
                    .then(function (dados) {
                        $scope.ddds = dados;
                    });
            },
            //template: "<select name=\"{{name}}\" class=\"form-control\" ng-model=\"model\" required ng-options=\"r.DDD as r.DDD for r in (ddds|filter:{Regional:regionalField})\"></select>"            
            template: "<md-select name=\"{{name}}\" ng-change=\"change()\" required ng-model=\"model\" aria-label=\"Regional\"><md-option ng-repeat=\"d in (ddds|filter:{Regional:regionalField}:true)\" ng-value=\"d.DDD\">{{d.DDD}}</md-option></md-select>"            
        };
    })
    .directive("selectMicroRegioes", function () {
        return {
            restrict: "E",
            scope: {
                name: "@",
                model: "=ngModel",
                dddField: "=ddd"
            },
            controller: function ($scope, MicroRegiaoService) {
                $scope.microregioes = [];
                MicroRegiaoService.listar()
                    .then(function (dados) {
                        $scope.microregioes = dados;
                    });
            },
            //template: "<select name=\"{{name}}\" class=\"form-control\" ng-model=\"model\" required ng-options=\"r.id as r.MicroRegiao for r in (microregioes|filter:{DDD:dddField})\"></select>"
            template: "<md-select name=\"{{name}}\" required ng-model=\"model\" aria-label=\"Regional\"><md-option ng-repeat=\"m in (microregioes|filter:{DDD:dddField})\" ng-value=\"m.id\">{{m.MicroRegiao}}</md-option></md-select>"
        }
    })
    .directive('selectConcorrenciaDistribuidores', function () {
        var ddo = {};
        ddo.restrict = 'E';
        ddo.scope = {
            name: '@name',
            model: '=ngModel',
            change: '&'
        };
        ddo.template = "<md-select name=\"{{name}}\" required ng-change=\"change()\" ng-model=\"model\" aria-label=\"Distribuidores\"><md-option ng-repeat=\"dist in concorrenciaDistribuidores\" ng-value=\"dist.Id\">{{dist.Distribuidor}}</md-option></md-select>";
        ddo.controller = function ($scope, WebAPI) {
            $scope.concorrenciaDistribuidores = [];
            WebAPI.getEntidadeApi("concorrencia-distribuidores")
                .then(function (dados) {
                    $scope.concorrenciaDistribuidores = dados.data;
                })                
                .catch(function (erro) {
                    console.log(erro);
                });
        }
        return ddo;
    })
    ;