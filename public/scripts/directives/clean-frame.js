angular.module('orbe.directives')
    .directive('cleanFrame', function ($timeout) {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                $timeout(function () {
                    console.log('Getting the frame...');
                }, 300);

            }
        };
    });