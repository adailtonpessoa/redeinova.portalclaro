angular.module('orbe.directives')
    .directive('menuEstatico', function () {
        var ddo = {};
        ddo.restrict = 'AE'; //(A)ttribute, (E)lement
        ddo.scope = {
            menu: '='
        };
        ddo.transclude = false;
        ddo.templateUrl = 'templates/ui/menu-estatico.html';
        return ddo;
    });