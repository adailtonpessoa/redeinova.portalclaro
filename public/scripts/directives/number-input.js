angular.module('orbe.directives')
    .directive('numberInput', function ($q) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {
                console.log('adding directive');
                elm.on('keypress', function ($event) {
                    if ($event.which < 48 || $event.which > 57) {
                        $event.stopPropagation();
                        $event.preventDefault();
                        if ($event.cancelBubble)
                            $event.cancelBubble = true
                        console.log('Descatado');
                    }
                });
                console.log('adding directive');
            }
        };
    });