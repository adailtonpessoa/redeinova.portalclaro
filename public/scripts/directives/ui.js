angular.module('orbe.directives.ui', [])
.directive('menuToggle', function($window) {
  return {
    scope: {
      section: '='
    },
    templateUrl: 'templates/ui/menu-toggle.html',
    link: function($scope, $element) {
      var controller = $element.parent().controller();

      $scope.isOpen = function(section) {
        //console.log($scope.section);
        //console.log('Is Open? ' + controller.isOpen($scope.section));
        //return controller.isOpen(section);
        return $scope.$root.currentPage == section.name || section.pages.some(function(p){
            return $scope.isSubSectionSelected(p);  
        });
      };
      $scope.toggle = function(section) {
        //controller.toggleOpen(section);
        if($scope.$root.currentPage == section.name){
          $scope.$root.currentPage = '';
        }
        else{
          $scope.$root.currentPage = section.name;
        }

         $element.parent().parent().find('.item-toggled').removeClass('item-toggled');
        if($scope.isOpen(section)){ 
          $scope.$root.currentPage = section.name;
          //console.log($element.parent());
          //$element.addClass('item-toggled');
          $element.parent().addClass('item-toggled');
        }
        else{
           $scope.$root.currentPage ='';
          $element.parent().removeClass('item-toggled');
          //$element.removeClass('item-toggled');
        }  
      };

      $scope.avoidClick = function($event, url, section){
          if ($event.stopPropagation) 
              $event.stopPropagation();
          $event.cancelBubble = true;
          $event.returnValue = false;
          $scope.$root.currentPage = section.name;
          $window.location.href = '#/' + url;
      };
      $scope.clickSubPage = function(page){
        return $scope.$root.currentSubPage == page.name; 
      };

      $scope.isSectionSelected = function(section){
        //console.log(section);
        return $scope.$root.currentPage == section.name;
      };

      $scope.isSubSectionSelected = function(page){
        //console.log(page);
        return $scope.$root.currentSubPage == page.name;
      };

      var parentNode = $element[0].parentNode.parentNode.parentNode;
      if(parentNode.classList.contains('parent-list-item')) {
        var heading = parentNode.querySelector('h2');
        $element[0].firstChild.setAttribute('aria-describedby', heading.id);
      }
    }
  };
})
.directive('menuLink', function() {
  return {
    scope: {
      page: '='
    },
    templateUrl: 'templates/ui/menu-link.html',
    link: function($scope, $element) {
      var controller = $element.parent().controller();

      $scope.isSelected = function() {
        return controller.isSelected($scope.section);
      };

      $scope.focusSection = function() {
        // set flag to be used later when
        // $locationChangeSuccess calls openPage()
        controller.autoFocusContent = true;
      };
    }
  };
})