(function () {
  'use strict';
  angular
      .module('orbe.directives.ui')
      .directive('loading', function ($q) {

        var controller = ['$scope', '$q', '$mdDialog', '$timeout', function customInputController($scope, $q, $mdDialog, $timeout){
            
        }];

        function linkFunction(scope, element, attrs, controllers){
            var loading = attrs.sgrLoading;
            element.parent()
                .append("<div class='row-loading-container ng-hide' layout='row' layout-align='center center'><img src='imagens/grid-loading.gif'/></div>");
            
            scope.$parent.$watch(loading, function(val){
                if(val){
                    element.addClass('ng-hide');
                    element.parent().find('.row-loading-container').removeClass('ng-hide');
                }
                else{
                    element.removeClass('ng-hide');
                    element.parent().find('.row-loading-container').addClass('ng-hide');
                }
            });
            console.log('Loading quando: ' +  attrs.sgrLoading);
        }  

        return {
            restrict : 'A',
            scope: {    
            },
            link : linkFunction,
            controller : controller,
            controllerAs: 'sgrLoading',
        };
    })
    .directive('fieldLoading', function ($q, $timeout) {

        function linkFunction(scope, element, attrs, controllers){
            $timeout(function(){
                 var loading = attrs.fieldLoading;
                element.children()
                    .append("<div class='row-loading-container ng-hide' layout='row' layout-align='center center'><img src='imagens/row-loading.gif'/></div>");
                
                scope.$watch(loading, function(val){
                    
                    if(val){
                        element.find(":first-child").children().eq(1).addClass('ng-hide');
                        element.find('.row-loading-container').removeClass('ng-hide');
                    }
                    else{
                        element.find(":first-child").children().eq(1).removeClass('ng-hide');
                        element.find('.row-loading-container').addClass('ng-hide');
                    }
                });
            }, 10);
           
        }  

        return {
            restrict : 'A',
            link : linkFunction,
        };
    });
})(); 