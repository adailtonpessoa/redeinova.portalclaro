angular.module('orbe.directives.slideform',[])
.directive('slideForm', ['$timeout', '$log', '$mdSidenav', '$window', '$q', function ($timeout, $log, $mdSidenav, $window,$q) {

	var linker = function (scope, element, attr) {

		scope.closeWindow = function(){
			$mdSidenav(scope.window)
	          .toggle()
	          .then(function () {
	          	$window.focus();
	          	element.find('.hidden_focus_input').focus();
      		});
		};

		scope.$parent.close = function(windowName){
			if($mdSidenav(windowName).isOpen())
				return $mdSidenav(windowName || scope.window)
		          .toggle()
		          .then(function () {
		          	$window.focus();
		          	element.find('.hidden_focus_input').focus();
	      		});
		    return $q.resolve();
		};

    	scope.$parent.openCloseWindow = function() {

    		var isClosing = windowName && $mdSidenav(windowName).isOpen();

    		if(!isClosing) {

	    		var windowName, item;

	    		arguments = Array.prototype.slice.call(arguments);

	    		if(arguments.length > 0){

	    			if(typeof(arguments[0]) === 'string')
	    				windowName = arguments[0];

					else if(typeof(arguments[0]) === 'object')
						item = arguments[0];

	    			if(!item && typeof(arguments[1]) === 'object')
	    				item = arguments[1];
	    		}

	    		windowName = windowName || scope.window;

	    		//$log.debug(windowName);
	    		//$log.debug(item);

	    		scope.dataItem = item;
    		}

			$mdSidenav(windowName).toggle().then(function() {

	          	if(isClosing)
	          		alert('closing...');
      		});
		};
	};

	return {
	    restrict: 'E',
	    scope : {
	    	window : "@",
	    },
	    controller : "@",
	    name : "windowController",
	    template : function(elem, attr){
	    	return '<md-sidenav class="md-sidenav-right md-whiteframe-z2" md-component-id="' + (attr.window || '')  + '">'
	    	+  	'<md-toolbar class="md-theme-light">'
	    	+	'<div layout-gt-sm="row" layout-align="none center">'
	    	+	'<h1 class="md-toolbar-tools">' + attr.title + '</h1>'
	    	+	'<md-button class="md-fab md-mini custom-fab" aria-label="clear" ng-click="closeWindow()" >'
	    	+	'<md-icon md-svg-icon="imagens/ic_clear_white_48px.svg"></md-icon>'
	        +	'</md-button>'
	    	+	'</div>'
	    	+  	'</md-toolbar>'
	    	+  	'<div ng-include="\'pages/' + attr.template + '.html\'"></div>'
	    	+ '<input class="hidden_focus_input" type="text" ng-hide="true" />'
	    	+ '</md-sidenav>';
	    },
	    link: linker
	};
}]);

