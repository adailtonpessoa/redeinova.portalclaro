angular.module('orbe.directives')
    .directive('handleKeys', function () {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                document.body.addEventListener('keyup', function (ev) {
                    console.log(ev);
                    if (ev.keyCode == 27) {
                        $scope.$root.leaveFullscreen();
                    }
                });

                document.getElementById('myframe').contentWindow.document.body.addEventListener('keyup', function (ev) {
                    console.log(ev);
                    if (ev.keyCode == 27) {
                        $scope.$root.leaveFullscreen();
                    }
                });
            }
        };
    });