angular.module("orbe.services")
    .factory("usuariosOrbessoService", function (WebAPI, $q) {
        var service = {};
        service.listar = function () {
            return $q(function (resolve, reject) {
                WebAPI.getEntidadeApiComQueryString("usuarios-orbesso", "disponiveis=S")
                    .then(function (dados) {
                        resolve(dados.data);
                    })
                    .catch(function (erro) {
                        console.log(erro);
                    })
            });
        }
        return service;
    });