angular.module("orbe.services")
    .factory("GruposPermissoesService", ["$q", "WebAPI", function ($q, WebAPI) {
        var service = {};
        service.listar = function() {
            return $q(function(resolve, reject) {
                WebAPI.getEntidadeApi("grupos-permissoes")
                .then(function(dados) {
                    resolve(dados.data);
                }).catch(function(erro) {
                    console.log(erro);
                })
            });
        }
        return service;
    }]);