angular.module("orbe.services")
    .factory("FuncionarioService", ["WebAPI", "$q", "$filter", "$rootScope",
        function (WebAPI, $q, $filter, $rootScope) {
            var service = {};

            service.listar = function () {
                return $q(function (certo, errado) {
                    WebAPI.getEntidadeApi("funcionarios")
                        .then(function (dados) {
                            certo(dados.data);
                        })
                        .catch(function (erro) {
                            errado(erro.data.msg);
                        })
                });
            }

            service.excluir = function (funcionario) {
                return $q(function (sucesso, falha) {
                    WebAPI.removerEntidade("funcionarios", funcionario.Id)
                        .then(function (dado) {
                            var retorno = dado.data[0];
                            if (retorno.error == "false")
                                sucesso();
                            else
                                falha();
                        })
                        .catch(function (erro) {
                            console.log(erro);
                        });
                });
            }

            service.salvar = function (objeto) {
                var funcionarioApi = {
                    Id: (objeto.Id) ? objeto.Id : 0,
                    IdRegional: objeto.IdRegional,
                    NomeCompleto: objeto.NomeCompleto,
                    IdCargo: objeto.IdCargo,
                    Matricula: objeto.Matricula,
                    CPF: objeto.CPF,
                    RG: objeto.RG,
                    TelefoneComercial: objeto.TelefoneComercial || null,
                    Celular: objeto.Celular,
                    DataNascimento: $filter("date")(objeto.DataNascimento, "yyyy-MM-dd hh:mm:ss") || null,
                    Email: objeto.Email,
                    Situacao : objeto.Situacao || "Inativo",
                    UsuarioOrbesso: objeto.UsuarioOrbesso || null ,
                    GrupoPermissao: objeto.GrupoPermissao || null 
                };

                var pl = {
                    id: (objeto.Id) ? objeto.Id : 0,
                    list: [funcionarioApi]
                };

                return $q(function (sucesso, falha) {

                    var eNovoRegistro = !(pl.id != 0);
                    var requisicao = {};

                    if (!eNovoRegistro)
                        requisicao = WebAPI.putEntidadeApi("funcionarios", pl)
                    else
                        requisicao = WebAPI.postEntidadeApi("funcionarios", pl)

                    requisicao
                        .then(function (data) {
                            var retorno = data.data[0];
                            if (retorno.erro == "false") {
                                $rootScope.$broadcast("voltarTelaPrincipal");
                                //console.log("Passou pelo broadcast");
                                sucesso({ sucesso: true });                                
                            }
                            else
                                falha({ sucesso: false, mensagem: retorno.msg });
                        }).catch(function (erro) {
                            falha({ sucesso: false, mensagem: erro.data.msg });
                        });;
                });
            }
            return service;
        }]);