angular.module('orbe.services.webAPI', [])
	.factory('WebAPI', ['$window', '$rootScope', 'CONFIG', 'USER', '$http', '$q', 'jwtHelper', '$rootScope',
		function ($window, $rootScope, CONFIG, USER, $http, $q, jwtHelper, $rootScope) {

			var api_url = CONFIG.API_URL;
			var auth_url = CONFIG.AUTH_URL;

			if ($window.location.host.indexOf('redeinova') != -1) {
				//api_url += 'portal-resultados/';
			}

			function handleNetworkError(res) {

				console.log('Handling Error');
				console.log(res);

				return $q.reject(res.data);
			}

			var online = true;

			$window.addEventListener("offline", function () {
				online = false;
			}, false);

			$window.addEventListener("online", function () {
				online = true;
			}, false);

			function getDefaultHeaders() {

				if (!online)
					throw exception('Acesso a internet indisponível. Cheque a sua conectividade e tente novamente.')

				USER.restore();

				if (!USER.signature)
					throw exception('Não autorizado');

				return {
					params: null,
					headers: { 'Authorization': 'Signature ' + USER.signature }
				};
			}


			function signin(signature) {
				try {
					if (signature.data)
						signature = signature.data;
					USER.signature = signature;

					var tokenPayload = jwtHelper.decodeToken(USER.signature);

					USER.name = tokenPayload.usuario.nome;
					USER.login = tokenPayload.usuario.login;

					USER.save();

					$rootScope.$broadcast("logou");
					return $q.when({ signature: signature });
				}
				catch (e) {
					return $q.reject("Usuário e/ou senha inválido(os)");
				}

			}

			function setTicket(res) {
				console.log('Setting ticket');
				console.log(res);


				USER.ticket = res.data.ticket;
				USER.save();

				return $q.when(USER.ticket);
			}

			function atualizarPermissoes() {
				return getEntidadeApi("permissoes");
			}

			function atualizarPermissoesSuccess(dados) {
				USER.permissoes = dados.data[0];
				USER.save();
				
				return $q.when();
			}

			function auth(login, password) {

				return $http.post(auth_url, { login: login, senha: password })
					.then(signin)
					.then(atualizarPermissoes)
					.then(atualizarPermissoesSuccess)
					.catch(handleNetworkError);
			}

			function getTicket() {
				var options = getDefaultHeaders();
				//return $q.when('TICKET');
				return $http.get('ticket', options)
					.then(setTicket)
					.catch(handleNetworkError);
			}

			function getMenu() {
				var options = getDefaultHeaders();
				USER.restore();
				return $http.get('api/menu?login=' + USER.login, options);
			}

			function getEntidadeApi(entidade) {
				var options = getDefaultHeaders();
				USER.restore();
				return $http.get("api/" + entidade + "?__debug=true&usuario=" + USER.login, options);
			}

			function getEntidadeApiComSequencial(entidade, sequencial) {
				var options = getDefaultHeaders();
				USER.restore();
				return $http.get("api/" + entidade + "?sequencial=" + sequencial + "&limite=1&__debug=true&usuario=" + USER.login, options);
			}

			function putEntidadeApi(entidade, objeto) {
				$rootScope.$broadcast("crudVoltar");
				var options = getDefaultHeaders();
				USER.restore();

				objeto.usuario = USER.login;

				return $http.put("api/" + entidade + "?__debug=true", objeto, options);
			}

			function postEntidadeApi(entidade, objeto) {
				$rootScope.$broadcast("crudVoltar");
				var options = getDefaultHeaders();
				USER.restore();

				objeto.usuario = USER.login;

				return $http.post("api/" + entidade + "?__debug=true", objeto, options);
			}

			function getEntidadeApiComQueryString(entidade, querystring) {
				var options = getDefaultHeaders();
				USER.restore();
				return $http.get("api/" + entidade + "?__debug=true&" + querystring + "&usuario=" + USER.login, options);
			}

			function removerEntidade(entidade, sequencial) {
				var options = getDefaultHeaders();
				USER.restore();
				return $http.delete("api/" + entidade + "?sequencial="+ sequencial +"&__debug=true&usuario=" + USER.login, options);
			}

			function deleteEntidade(entidade, data, credentials) {
				var headers = credentials || {
					'Authorization': 'Signature ' + USER.signature,
					"Content-Type": "application/json;charset=utf-8"
				};

				data.usuario = USER.login;

				return $http({ method: 'delete', url: "api/" + entidade, data : data, headers: headers })
			}

			return {
				auth: auth,
				getTicket: getTicket,
				getMenu: getMenu,
				getEntidadeApi: getEntidadeApi,
				putEntidadeApi: putEntidadeApi,
				postEntidadeApi: postEntidadeApi,
				getEntidadeApiComQueryString: getEntidadeApiComQueryString,
				getEntidadeApiComSequencial : getEntidadeApiComSequencial,
				removerEntidade: removerEntidade,
				deleteEntidade : deleteEntidade
			};

		}]);

