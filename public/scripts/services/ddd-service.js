angular.module("orbe.services")
    .factory("DDDService", ["$q", "WebAPI", function ($q, WebAPI) {
        var service = {};
        service.listar = function() {
            return $q(function(resolve, reject) {
                WebAPI.getEntidadeApi("ddds")
                .then(function(dados) {
                    resolve(dados.data);
                }).catch(function(erro) {
                    console.log(erro);
                })
            });
        }
        return service;
    }]);