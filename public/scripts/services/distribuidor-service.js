angular.module("orbe.services")
    .factory("DistribuidorService", function (WebAPI, $q) {
        var service = {};
        service.listar = function () {
            return  WebAPI.getEntidadeApiComQueryString("distribuidor", "limite=500")
            .then(function (dados) {
                return $q.when(dados.data);
            })
            .catch(function (erro) {
                console.log(erro);
                return $q.when(erro);
            });
        }

        service.buscar = function (pagina, quantidade) {
            var sequencial = (pagina -1) * quantidade;
            return WebAPI.getEntidadeApiComQueryString("busca-distribuidor", "sequencial=" + sequencial + "&limite=" + quantidade);
        }

        service.excluir = function(distribuidor){
            console.log('removendo distribuidor...');
            console.log(distribuidor);
            return WebAPI.deleteEntidade("distribuidor", {  list : [ { sequencial : distribuidor.Id } ] });
        }

        service.salvar = function (distribuidor) {
            console.log('Distribuidor Salvar');
            console.log(distribuidor);
            // distribuidor.list[0] 
            var p = distribuidor.list[0].Id ? WebAPI.putEntidadeApi : WebAPI.postEntidadeApi;

            return p("distribuidor", distribuidor);
            
        }
        return service;
    });