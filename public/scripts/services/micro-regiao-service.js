angular.module("orbe.services")
    .factory("MicroRegiaoService", function (WebAPI, $q) {
        var service = {};
        service.listar = function() {
            return $q(function(resolve, reject) {
                WebAPI.getEntidadeApi("microregiao")
                .then(function(dados) {
                    resolve(dados.data);
                }).catch(function(erro) {
                    console.log(erro);
                });
            });
        }
        return service;
    });