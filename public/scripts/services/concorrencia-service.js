angular.module("orbe.services")
    .factory("ConcorrenciaService", function (WebAPI, $q) {
        var service = {};
        service.listar = function () {
            return $q(function (resolve, reject) {
                WebAPI.getEntidadeApiComQueryString("concorrencias", "completo=N")
                    .then(function (dados) {
                        resolve(dados.data);
                    })
                    .catch(function (erro) {
                        console.log(erro);
                    })
            });
        }

        service.listarCompleto = function () {
            return $q(function (resolve, reject) {
                WebAPI.getEntidadeApiComQueryString("concorrencias", "completo=S&sequencial=0")
                    .then(function (dados) {
                        resolve(dados.data);
                    })
                    .catch(function (erro) {
                        console.log(erro);
                    })
            });
        }

        service.buscar = function (id) {
            return $q(function (resolve, reject) {
                WebAPI.getEntidadeApiComQueryString("concorrencias", "completo=S&sequencial=" + id)
                    .then(function (dados) {
                        var retorno = dados.data;
                        var dadosTransformados = {
                            IdRegional: retorno[0].IdRegional,
                            DDD: retorno[0].DDD,
                            IdMicroRegiao: retorno[0].IdMicroRegiao,
                            Regional: retorno[0].Regional,
                            MicroRegiao: retorno[0].MicroRegiao,
                            dados: retorno
                        };
                        resolve(dadosTransformados);
                    })
                    .catch(function (erro) {
                        console.log(erro);
                    })
            });
        }

        service.salvar = function (concorrencia) {
            return $q(function (resolve, reject) {

                var concorrenciaApi = {};
                concorrenciaApi.IdRegional = concorrencia.IdRegional;
                concorrenciaApi.DDD = concorrencia.DDD;
                concorrenciaApi.IdMicroRegiao = concorrencia.IdMicroRegiao;
                concorrenciaApi.dados = [];
                concorrenciaApi.usuario = concorrencia.usuario;

                for (var i = 0; i < concorrencia.dados.length; i++) {
                    var dado = {};
                    dado.Operadora = concorrencia.dados[i].Operadora;
                    dado.Nome = concorrencia.dados[i].Nome;
                    dado.Capilaridade = concorrencia.dados[i].Capilaridade;
                    dado.FaturamentoMedioMensal = concorrencia.dados[i].FaturamentoMedioMensal;
                    dado.GrossMedioMensal = concorrencia.dados[i].GrossMedioMensal;
                    dado.Exclusivo = concorrencia.dados[i].Exclusivo;
                    dado.MargemFixa2 = concorrencia.dados[i].MargemFixa2;
                    dado.MargemVariavel = concorrencia.dados[i].MargemVariavel;
                    dado.MargemProgramaExcelencia = concorrencia.dados[i].MargemProgramaExcelencia;
                    dado.IdConcorrenciaDistribuidores = concorrencia.dados[i].IdConcorrenciaDistribuidores;                    
                    concorrenciaApi.dados.push(dado);
                }
                
                console.log(concorrenciaApi);

                WebAPI.postEntidadeApi("concorrencias", concorrenciaApi)
                    .then(function (dados) {
                        var retorno = dados.data[0];
                        if (retorno.erro == "false") {
                            resolve(retorno);
                        } else {
                            reject(retorno.msg);
                        }
                    })
                    .catch(function (erro) {
                        console.log(erro);
                        reject(erro);
                    });

            });
        }

        return service;
    });