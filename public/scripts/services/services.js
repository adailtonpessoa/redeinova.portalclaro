angular.module('orbe.services', ['orbe.services.webAPI'])
.factory('USER', [function(){
	return {
		signature 	: '',
		name 		: '',
		login		: '',
		save		: function(){
			sessionStorage.userInfo = angular.toJson({ 
				signature 	: this.signature,
				name 		: this.name,
				login		: this.login,
				ticket		: this.ticket,
				menu		: this.menu
			});
		},
		restore		: function(){
			if (sessionStorage.userInfo){
            	var info 		=  angular.fromJson(sessionStorage.userInfo);
            	this.signature 	= info.signature;
            	this.name 		= info.name;
            	this.login 		= info.login;
				this.ticket     = info.ticket;
				this.menu		= info.menu;
        	}
		},
		clear 		: function(){
			sessionStorage.removeItem("userInfo");
		},
		permissoes: {
			consultar: true,
			editar: true,
			admin: true
		}
	};
}])