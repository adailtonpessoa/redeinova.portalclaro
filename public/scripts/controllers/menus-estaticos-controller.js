angular.module("orbe.controllers")
    .controller('MenusEstaticosController', function ($scope) {
        $scope.menus = [{
            menu: 'Cadastros',
            submenus: [{
                menu: "Funcionários",
                url: "#/funcionarios"
            },{
                menu: "Concorrências",
                url: "#/concorrencias"
            }]
        }];
    });