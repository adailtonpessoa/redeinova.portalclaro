angular.module("orbe.controllers")
    .controller('LogoutController',
    function ($scope, $window, USER, $timeout) {
        USER.clear();
        $timeout(function () {
            var fullUrl = $window.location.href.toLowerCase();
            fullUrl = fullUrl.substring(0, fullUrl.indexOf('#'));
            $window.location.href = fullUrl;
        }, 200);
    });