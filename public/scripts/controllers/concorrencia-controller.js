angular.module('orbe.controllers')
    .controller("ConcorrenciasController", ["$scope", "ConcorrenciaService", "USER",
        function ($scope, ConcorrenciaService, USER) {
            $scope.concorrencias = "";            
            $scope.permissoes = USER.permissoes;
            
            ConcorrenciaService.listar()
                .then(function (dados) {
                    // console.log(dados);
                    $scope.concorrencias = dados;
                });

            $scope.concorrenciasCompleto = "";

            ConcorrenciaService.listarCompleto()
                .then(function (dados) {
                    // console.log(dados);
                    $scope.concorrenciasCompleto = dados;
                });
            
            $scope.removeAcentos = function (texto) {
                if (!angular.isDefined(texto)) { return; }
                var accent = [
                    /[\300-\306]/g, /[\340-\346]/g, // A, a
                    /[\310-\313]/g, /[\350-\353]/g, // E, e
                    /[\314-\317]/g, /[\354-\357]/g, // I, i
                    /[\322-\330]/g, /[\362-\370]/g, // O, o
                    /[\331-\334]/g, /[\371-\374]/g, // U, u
                    /[\321]/g, /[\361]/g, // N, n
                    /[\307]/g, /[\347]/g, // C, c
                ],
                    noaccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];

                for (var i = 0; i < accent.length; i++) {
                    texto = texto.replace(accent[i], noaccent[i]);
                }

                return texto;
            };

            $scope.exportData = function () {                
                var blob = new Blob([document.getElementById('exportable').innerHTML], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, "ReportConcorrencias.xls");
            };
        }])

    .controller('ConcorrenciaController', ['$scope', 'ConcorrenciaService', '$routeParams', "$window", "USER", "WebAPI",
        function ($scope, ConcorrenciaService, $routeParams, $window, USER, WebAPI) {
            $scope.mensagem = "";

            $scope.concorrencia = {
                dados: [
                    { Operadora: "VIVO" },
                    { Operadora: "TIM" },
                    { Operadora: "OI" }
                ]
            }

            if (!USER.permissoes.editar) {
                $window.location.href = "#/concorrencias";
            }

            if ($routeParams.id) {
                $scope.loading = true;
                ConcorrenciaService.buscar($routeParams.id)
                    .then(function (dado) {
                        console.log(dado);
                        $scope.concorrencia = dado;
                        $scope.loading = false;
                    })
                    .catch(function (erro) {
                        $scope.showError(erro);
                        $scope.loading = false;
                    });
            }

            $scope.regionaisChange = function () {
                // console.log("Mudança de regonal" + $scope.concorrencia.IdRegional);
                $scope.concorrencia.DDD = null;
                $scope.concorrencia.IdMicroRegiao = null;
            }

            $scope.dddsChange = function () {
                // console.log("Mudança de ddd" + $scope.concorrencia.DDD);                
                $scope.concorrencia.IdMicroRegiao = null;
            }

            $scope.submeter = function () {

                // console.log($scope.concorrencia);

                $scope.concorrencia.dados.map(function (e) {
                    e.Exclusivo = e.Exclusivo;
                    e.Capilaridade = parseInt(e.Capilaridade);
                    e.FaturamentoMedioMensal = parseFloat(e.FaturamentoMedioMensal);
                    e.GrossMedioMensal = parseFloat(e.GrossMedioMensal);
                    e.MargemFixa2 = parseFloat(e.MargemFixa2);
                    e.MargemProgramaExcelencia = parseFloat(e.MargemProgramaExcelencia);
                    e.MargemVariavel = parseFloat(e.MargemVariavel);
                    e.Nome = "";
                    e.IdConcorrenciaDistribuidores = parseInt(e.IdConcorrenciaDistribuidores);

                    delete e.IdMicroRegiao;
                    delete e.IdOperadora;
                    delete e.IdRegional;
                    delete e.id;
                    delete e.DDD;

                });

                ConcorrenciaService.salvar($scope.concorrencia)
                    .then(function (dados) {
                        $scope.showMessage("Concorrência salva com sucesso");
                        $window.location.href = '#/concorrencias'
                    }).catch(function (erro) {
                        if (erro) {
                            $scope.showError(erro);
                        } else {
                            $scope.showError("Falha ao salvar a concorrencia");
                        }
                    });
            }
        }])
        .filter('searchFor2', function () {
            return function (arr, searchString) {
                if (!searchString) {
                    return arr;
                }
                var result = [];
                searchString = searchString.toUpperCase();
                angular.forEach(arr, function (item) {
                    if (item.Regional.toUpperCase().indexOf(searchString) !== -1 || item.DDD.toUpperCase().indexOf(searchString) !== -1 || item.MicroRegiao.toUpperCase().indexOf(searchString) !== -1) {
                        result.push(item);
                    }
                });
                return result;
            };
        });

