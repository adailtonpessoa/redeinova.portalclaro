angular.module('orbe.controllers.dash', [])
.controller('DashController', ['$scope', '$log', '$mdSidenav', '$mdToast', '$timeout', '$q', 'WebAPI', '$routeParams', 'CONFIG', 'USER',
    function($scope, $log, $mdSidenav,$mdToast,$timeout,  $q, WebAPI, $routeParams, CONFIG, USER){

    $scope.$root.$watch('url', function(url){
        console.log('Nova url');
        $scope.url = url;
    });

   function show(ticket){
       $scope.url = CONFIG.RESOLUTO_SERVER + '/' + ticket + '/views/' + $routeParams.view;
       //$scope.url = "http://www.fanstudios.com.br";
       console.log('View URL: ' + $scope.url);
       $scope.loading = false;
   }

    function load(){
        console.log('Getting ticket');
        $scope.loading = true;
        WebAPI.getTicket()
        .then(show);
   }
    
    load();
    
}]);