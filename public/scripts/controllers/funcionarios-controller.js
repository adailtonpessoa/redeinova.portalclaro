angular.module("orbe.controllers")
    .controller('FuncionariosController', ['$scope', 'FuncionarioService', '$mdDialog', 'USER',
        function ($scope, FuncionarioService, $mdDialog, USER) {
            $scope.funcionarios = [];
            $scope.loading = true;
            $scope.permissoes = USER.permissoes;

            FuncionarioService.listar()
                .then(function (dados) {
                    $scope.funcionarios = dados;
                    // $scope.showMessage('Ok');
                    $scope.loading = false;
                })
                .catch(function (erro) {
                    console.log(erro);
                    $scope.showError(erro);
                    $scope.loading = false;
                });

            $scope.removeAcentos = function (texto) {
                if (!angular.isDefined(texto)) { return; }
                var accent = [
                    /[\300-\306]/g, /[\340-\346]/g, // A, a
                    /[\310-\313]/g, /[\350-\353]/g, // E, e
                    /[\314-\317]/g, /[\354-\357]/g, // I, i
                    /[\322-\330]/g, /[\362-\370]/g, // O, o
                    /[\331-\334]/g, /[\371-\374]/g, // U, u
                    /[\321]/g, /[\361]/g, // N, n
                    /[\307]/g, /[\347]/g, // C, c
                ],
                    noaccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];

                for (var i = 0; i < accent.length; i++) {
                    texto = texto.replace(accent[i], noaccent[i]);
                }

                return texto;
            };

            $scope.exportData = function () {
                var blob = new Blob([document.getElementById('exportable').innerHTML], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, "ReportFuncionarios.xls");
            };

            $scope.confirmarExclusao = function (item, $event) {
                var confirm = $mdDialog.confirm()
                    .title('Confirmar')
                    .textContent('Deseja realmente excluir o funcionário?')
                    .ariaLabel('Confirmar')
                    .targetEvent($event)
                    .clickOutsideToClose(true)
                    .ok('Excluir')
                    .cancel('Cancelar');

                $mdDialog.show(confirm)
                    .then(function () {
                        console.log('Excluir item');

                        FuncionarioService.excluir(item)
                            .then(function () {
                                var index = $scope.funcionarios.indexOf(item);
                                $scope.funcionarios.splice(index, 1);
                            });

                    }, function () {
                        console.log('Cancelou...');
                        //cancelou
                    });
            };
        }])
    .filter('searchFor', function () {
        return function (arr, searchString) {
            if (!searchString) {
                return arr;
            }

            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function (item) {
                if (item.Regional.toLowerCase().indexOf(searchString) !== -1 || item.NomeCompleto.toLowerCase().indexOf(searchString) !== -1 || item.Matricula.toLowerCase().indexOf(searchString) !== -1) {
                    result.push(item);
                }
            });
            return result;
        };
    });