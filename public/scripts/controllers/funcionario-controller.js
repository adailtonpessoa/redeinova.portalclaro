angular.module('orbe.controllers')
    .controller('FuncionarioController', ["$scope", "$routeParams", "$q", "WebAPI", "FuncionarioService", 
                                            "$window", "usuariosOrbessoService", "GruposPermissoesService", "USER",
        function ($scope, $routeParams, $q, WebAPI, FuncionarioService, $window, usuariosOrbessoService, GruposPermissoesService, USER) {
            
            $scope.funcionario = {};
            $scope.usuariosOrbessoDisponiveis = [];
            $scope.gruposPermissoes = [];

            $scope.permissoes = USER.permissoes;

            $scope.formData = {
                regionais: [],
                cargos: []
            }

            function inicializarFormulario() {

                if(!USER.permissoes.editar) {
                    $window.location.href = "#/funcionarios";
                }

                $scope.loading = true;
                console.log('Route')
                console.log($routeParams);
                var date = new Date();
                $scope.maxDate = new Date(date.getUTCFullYear(),
                    date.getUTCMonth(),
                    date.getUTCDate(),
                    date.getUTCHours(),
                    date.getUTCMinutes(),
                    date.getUTCSeconds());

                if ($routeParams.id)
                    return WebAPI.getEntidadeApiComSequencial("funcionarios", $routeParams.id);

                return $q.when();
            }

            function prepararDadosIniciais(dados) {
                console.log('Dados do funcionário: ');
                console.log(dados);
                if (dados) {

                    var funcionario = dados.data[0];

                    if (funcionario.DataNascimento) {

                        var dataNascFunc = new Date(funcionario.DataNascimento);

                        funcionario.DataNascimento = new Date(dataNascFunc.getUTCFullYear(),
                            dataNascFunc.getUTCMonth(),
                            dataNascFunc.getUTCDate(),
                            dataNascFunc.getUTCHours(),
                            dataNascFunc.getUTCMinutes(),
                            dataNascFunc.getUTCSeconds());
                    }

                    $scope.funcionario = funcionario;
                }
                else {
                    $scope.funcionario = {};
                }
            }

            function finalizarInicializacao() {
                $scope.loading = false;
            }

            function deuErro(erro) {
                console.log(erro);
                $scope.showError(erro);
                $scope.loading = false;
            }

            function obterRegionais() {
                return WebAPI.getEntidadeApi("regionais");
            }

            function obterRegionaisSuccess(dados) {
                $scope.formData.regionais = dados.data;
                return $q.when();
            }

            function obterCargos() {
                return WebAPI.getEntidadeApi("cargos");
            }

            function obterCargosSuccess(dados) {
                $scope.formData.cargos = dados.data;
                return $q.when();
            }

            function obterUsuariosOrbesso() {
                return usuariosOrbessoService.listar();
            }

            function obterUsuariosOrbessoSuccess(dados) {
                if ($routeParams.id) 
                    dados.push({ Usuario: $scope.funcionario.UsuarioOrbesso });
                                
                $scope.usuariosOrbessoDisponiveis = dados;
                return $q.when();
            }

            function obterGruposPermissoes() {
                return GruposPermissoesService.listar();
            }

            function obterGruposPermissoesSuccess(dados) {
                $scope.gruposPermissoes = dados;
                return $q.when();
            }

            inicializarFormulario()
                .then(prepararDadosIniciais)
                .then(obterRegionais)
                .then(obterRegionaisSuccess)
                .then(obterCargos)
                .then(obterCargosSuccess)
                .then(finalizarInicializacao)
                .then(obterUsuariosOrbesso)
                .then(obterUsuariosOrbessoSuccess)
                .then(obterGruposPermissoes)
                .then(obterGruposPermissoesSuccess)
                .catch(deuErro);

            $scope.salvar = function () {
                if (!$scope.loading) {
                    $scope.loading = true;

                    $scope.funcionario.Id = $routeParams.id;

                    FuncionarioService.salvar($scope.funcionario)
                        .then(function (dados) {
                            $scope.loading = false;
                            console.log(dados);

                            $scope.showMessage("Funcionário salvo com sucesso");
                            $window.location.href = '#/funcionarios'

                        })
                        .catch(function (erro) {
                            $scope.loading = false;
                            $scope.showError("Erro ao tentar salvar o funcionário " + erro.mensagem);
                            console.log(erro);
                        });
                }
            };
        }])
    ;