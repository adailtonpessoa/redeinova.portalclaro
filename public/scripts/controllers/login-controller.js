angular.module("orbe.controllers")
    .controller('LoginController', function ($scope, WebAPI, $q, $window, $mdToast, USER) {

        $scope.loading = false;
        $scope.entity = {};

        function validateRequest() {
            $scope.loading = true;
            if (!$scope.entity.username)
                return $q.reject('Nome de usuário deve ser fornecido');
            if (!$scope.entity.password)
                return $q.reject('Senha deve ser fornecida');

            return $q.resolve();
        }

        function tryToLogin() {
            return WebAPI.auth($scope.entity.username, $scope.entity.password);
        }

        function redirectToMainPage(menu) {
            if (typeof (localStorage) != 'undefined') {
                if ($scope.entity.lembrarSenha) {
                    localStorage.username = $scope.entity.username;
                    localStorage.password = $scope.entity.password;
                }
                localStorage.lembrarSenha = $scope.entity.lembrarSenha;
            }


            var fullUrl = $window.location.href.toLowerCase();
            fullUrl = fullUrl.substring(0, fullUrl.indexOf('#'));

            $window.location = fullUrl + '#/home';
        }

        function getTicket() {
            //return WebAPI.getTicket();
            return $q.when('Ticket');
        }
        function getMenu() {
            return WebAPI.getMenu()
                .then(function (res) {

                    var menuItems = res.data;
                    $scope.$root.pages = [];
                    var sections = menuItems.filter(function (mi) {
                        return !mi.IdMenuAnterior;
                    })
                        .map(function (s) {
                            if (s.Caminho)
                                $scope.$root.pages.push({ url: '/dashboard/view/' + s.Caminho });

                            return {
                                id: s.Id,
                                name: s.Descricao,
                                type: s.Caminho ? 'link' : 'toggle',
                                icon: './imagens/' + s.Icone,
                                url: s.Caminho ? ('/dashboard/view/' + s.Caminho) : undefined,
                                pages: []
                            }
                        });

                    sections.forEach(function (s) {
                        $scope.$root.pages = $scope.$root.pages.concat(s.pages.map(function (p) {
                            return { url: '/dashboard/view/' + p.Caminho }
                        }));

                        s.pages = menuItems.filter(function (m) {
                            console.log(m.IdMenuAnterior + ' - ' + s.id);
                            return m.IdMenuAnterior == s.id;
                        })
                            .map(function (p) {
                                return {
                                    name: p.Descricao,
                                    url: '/dashboard/view/' + p.Caminho,
                                    className: p.Icone ? 'menu-icon-link' : 'menu-icon-bullet',
                                    icon: p.Icone ? './imagens/' + p.Icone : './imagens/bullet.svg'
                                };
                            });

                    });

                    sections.push({
                     
                        // className: 'menu-icon-link',
                        id: 999,
                        name: 'Cadastros',
                        type: 'toggle',
                        icon: './imagens/ic_insert_drive_file_white_24px.svg',
                        url: '#',
                        pages: [{
                                name: 'Funcionários',
                                url: '/funcionarios',
                                className:  'menu-icon-link',
                                icon: './imagens/ic_group_white_24px.svg'
                            },
                            {
                                name: 'Concorrência',
                                url: '/concorrencias',
                                className:  'menu-icon-link',
                                icon: './imagens/ic_timeline_white_24px.svg'
                            }
                            ,{
                                name: 'Distribuidor',
                                url: '/distribuidores',
                                className:  'menu-icon-link',
                                icon: './imagens/ic_local_shipping_white_24px.svg'
                            }
                        ]
                    });


                    USER.menu = sections;
                    var menuController = angular.element(document.querySelector("#main_menu")).scope();
                    menuController.sections = sections;

                    USER.save();
                    $scope.$root.isLoggedIn = true;
                    $scope.$root.sections = sections;

                    if ($scope.$root.pages.length) {
                        $window.location.href = '#' + $scope.$root.pages[0].url;
                    }

                    return $q.when();
                });
        }

        function error(err) {
            if (err.data)
                err = err.data;
            if (err.msg)
                err = err.msg;

            $scope.loading = false;
            delete USER.signature;
            USER.save();

            $mdToast.show(
                $mdToast.simple()
                    .textContent(err)
                    .position("true")
                    .hideDelay(5000)
            );
        }

        $scope.entrar = function () {
            validateRequest()
                .then(tryToLogin)
                .then(getTicket)
                .then(getMenu)
                .then(redirectToMainPage)
                .catch(error);
        };

        USER.restore();
        if (USER.signature) {
            $window.location = "#/home";
        }

        if (typeof (localStorage) != 'undefined' && localStorage.lembrarSenha == "true") {
            console.log('Lembrando da senha...');
            $scope.entity = {
                username: localStorage.username,
                password: localStorage.password,
                lembrarSenha: true
            };
        }
        else {
            $scope.entity = {
                username: null,
                lembrarSenha: false
            };
        }


    });