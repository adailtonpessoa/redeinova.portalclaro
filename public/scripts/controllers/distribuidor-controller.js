angular.module('orbe.controllers')
    .controller('DistribuidoresController', ["$scope", "$routeParams", "$q", "WebAPI", "DistribuidorService", "$mdDialog", "USER",
        function ($scope, $routeParams, $q, WebAPI, DistribuidorService, $mdDialog, USER) {
            $scope.distribuidores = [];
            // $scope.paginaSelecionada = 1;
            // $scope.tamanhoPagina = 10;
            // $scope.totalPaginas = 1;

            $scope.permissoes = USER.permissoes;

            DistribuidorService.listar()
                .then(function (dados) {
                    $scope.distribuidores = dados;
                    // $scope.showMessage('Ok');
                    $scope.loading = false;
                })
                .catch(function (erro) {
                    console.log(erro);
                    $scope.showError(erro);
                    $scope.loading = false;
                });
            
            $scope.removeAcentos = function (texto) {
                if (!angular.isDefined(texto)) { return; }
                var accent = [
                    /[\300-\306]/g, /[\340-\346]/g, // A, a
                    /[\310-\313]/g, /[\350-\353]/g, // E, e
                    /[\314-\317]/g, /[\354-\357]/g, // I, i
                    /[\322-\330]/g, /[\362-\370]/g, // O, o
                    /[\331-\334]/g, /[\371-\374]/g, // U, u
                    /[\321]/g, /[\361]/g, // N, n
                    /[\307]/g, /[\347]/g, // C, c
                ],
                    noaccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];

                for (var i = 0; i < accent.length; i++) {
                    texto = texto.replace(accent[i], noaccent[i]);
                }

                return texto;
            };

            $scope.exportData = function () {                
                var blob = new Blob([document.getElementById('exportable').innerHTML], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                });
                saveAs(blob, "ReportDistribuidores.xls");
            };

            // $scope.buscarPagina = function (pagina) {
            //     if (pagina > $scope.totalPaginas || pagina < 1)
            //         return;
            //     $scope.loading = true;
            //     DistribuidorService.buscar(pagina, $scope.tamanhoPagina)
            //         .then(function (dados) {
            //             $scope.loading = false;
            //             $scope.distribuidores = dados.data;
            //             if ($scope.distribuidores.length > 0) {
            //                 $scope.totalItens = $scope.distribuidores[0].qtde_total;
            //                 $scope.totalPaginas = Math.ceil($scope.totalItens / $scope.tamanhoPagina);
            //             }
            //             $scope.selectedPage = pagina;
            //             $scope.paginaSelecionada = pagina;
            //         })
            //         .catch(function (err) {
            //             console.log(err);
            //             $scope.loading = false;
            //         });
            // }

            $scope.confirmarExclusao = function (item, $event) {
                var confirm = $mdDialog.confirm()
                    .title('Confirmar')
                    .textContent('Deseja realmente excluir o distribuidor?')
                    .ariaLabel('Confirmar')
                    .targetEvent($event)
                    .clickOutsideToClose(true)
                    .ok('Excluir')
                    .cancel('Cancelar');

                $mdDialog.show(confirm)
                    .then(function () {
                        DistribuidorService.excluir(item)
                            .then(function () {
                                var index = $scope.distribuidores.indexOf(item);
                                $scope.distribuidores.splice(index, 1);
                            });
                    }, function () {
                        console.log('Cancelou...');
                    });
            };

            //$scope.buscarPagina(1);
        }])
    .controller('DistribuidorController', ["$scope", "$routeParams", "$q", "WebAPI", "DistribuidorService", "$mdDialog", "UTIL", "$filter", "$window", "USER",
        function ($scope, $routeParams, $q, WebAPI, DistribuidorService, $mdDialog, UTIL, $filter, $window, USER) {
            $scope.distribuidor = {};
            $scope.socios = [];
            $scope.integradores = [];

            $scope.gnSellIn = '';
            $scope.gnSellOut = '';

            $scope.formData = {
                estados: [],
                operadoras: [],
                posicoes: [
                    { Id: 1, Nome: 'Primeira' },
                    { Id: 2, Nome: 'Segunda' },
                    { Id: 3, Nome: 'Terceira' },
                    { Id: 4, Nome: 'Quarta' },
                    { Id: 5, Nome: 'Quinta' }
                ],
                integradores: [
                    { Id: 1, Nome: 'Integrador 1' },
                    { Id: 2, Nome: 'Integrador 2' },
                    { Id: 3, Nome: 'Integrador 3' }
                ]
            };

            if (!USER.permissoes.editar) {
                $window.location.href = "#/distribuidores";
            }

            function inicializarFormulario() {
                $scope.loading = true;
                console.log('Route')
                console.log($routeParams);

                var date = new Date();
                $scope.maxDate = new Date(date.getUTCFullYear(),
                    date.getUTCMonth(),
                    date.getUTCDate(),
                    date.getUTCHours(),
                    date.getUTCMinutes(),
                    date.getUTCSeconds());

                if ($routeParams.id)
                    return WebAPI.getEntidadeApiComSequencial("distribuidor", $routeParams.id);

                return $q.when();
            }

            function prepararDadosIniciais(dados) {
                if (dados) {
                    console.log('Dados do Distribuidor: ');
                    console.log(dados);

                    var distribuidor = dados.data[0];

                    if (distribuidor.DataDaEntrada) {

                        var dataEntrada = new Date(distribuidor.DataDaEntrada);

                        distribuidor.DataDaEntrada = new Date(dataEntrada.getUTCFullYear(),
                            dataEntrada.getUTCMonth(),
                            dataEntrada.getUTCDate(),
                            dataEntrada.getUTCHours(),
                            dataEntrada.getUTCMinutes(),
                            dataEntrada.getUTCSeconds());
                    }

                    if (distribuidor.IdGNSellIn > 0) {
                        $scope.gnSellIn = {
                            id: distribuidor.IdGNSellIn,
                            text: distribuidor.GNSellIn
                        };
                    }

                    if (distribuidor.IdGNSellOut > 0) {
                        $scope.gnSellOut = {
                            id: distribuidor.IdGNSellOut,
                            text: distribuidor.GNSellOut
                        };
                    }

                    $scope.distribuidor = distribuidor;
                }
                else {
                    $scope.distribuidor = {};
                }
            }

            function finalizarInicializacao() {
                $scope.loading = false;
            }

            function deuErro(erro) {
                console.log(erro);
                $scope.showError(erro);
                $scope.loading = false;
            }

            function obterOperadoras() {
                return WebAPI.getEntidadeApi("operadora");
            }

            function obterOperadorasSuccess(dados) {
                $scope.formData.operadoras = dados.data;
                return $q.when();
            }

            function obterIntegradores() {
                return WebAPI.getEntidadeApi("integrador");
            }

            function obterIntegradoresSuccess(dados) {
                console.log('integradores: ')
                console.log(dados)
                $scope.formData.integradores = dados.data;
                return $q.when();
            }

            function obterSocios() {
                if ($routeParams.id)
                    return WebAPI.getEntidadeApiComQueryString("distribuidor-socio", "distribuidor=" + $routeParams.id);
                return $q.when({ data: [] });
            }

            function obterSociosSuccess(dados) {
                console.log('socios: ')
                console.log(dados)
                $scope.socios = dados.data;
                return $q.when();
            }

            function obterIntegradoresDistribuidor() {
                if ($routeParams.id)
                    return WebAPI.getEntidadeApiComQueryString("distribuidor-integrador", "distribuidor=" + $routeParams.id);
                return $q.when({ data: [] });
            }

            function obterIntegradoresDistribuidorSuccess(dados) {
                console.log('integradores: ')
                console.log(dados)
                $scope.integradores = dados.data;
                return $q.when();
            }

            function obterEstados() {
                var estados = [
                    { Nome: 'ACRE', Id: 'AC' },
                    { Nome: 'ALAGOAS', Id: 'AL' },
                    { Nome: 'AMAPÁ', Id: 'AP' },
                    { Nome: 'AMAZONAS', Id: 'AM' },
                    { Nome: 'BAHIA', Id: 'BA' },
                    { Nome: 'CEARÁ', Id: 'CE' },
                    { Nome: 'DISTRITO FEDERAL', Id: 'DF' },
                    { Nome: 'ESPÍRITO SANTO', Id: 'ES' },
                    { Nome: 'GOIÁS', Id: 'GO' },
                    { Nome: 'MARANHÃO', Id: 'MA' },
                    { Nome: 'MATO GROSSO', Id: 'MT' },
                    { Nome: 'MATO GROSSO DO SUL', Id: 'MS' },
                    { Nome: 'MINAS GERAIS', Id: 'MG' },
                    { Nome: 'PARÁ', Id: 'PA' },
                    { Nome: 'PARAÍBA', Id: 'PB' },
                    { Nome: 'PARANÁ', Id: 'PR' },
                    { Nome: 'PERNAMBUCO', Id: 'PE' },
                    { Nome: 'PIAUÍ', Id: 'PI' },
                    { Nome: 'RIO DE JANEIRO', Id: 'RJ' },
                    { Nome: 'RIO GRANDE DO NORTE', Id: 'RN' },
                    { Nome: 'RIO GRANDE DO SUL', Id: 'RS' },
                    { Nome: 'RONDÔNIA', Id: 'RO' },
                    { Nome: 'RORAIMA', Id: 'RR' },
                    { Nome: 'SANTA CATARINA', Id: 'SC' },
                    { Nome: 'SÃO PAULO', Id: 'SÃO PAULO' },
                    { Nome: 'SERGIPE', Id: 'SE' },
                    { Nome: 'TOCANTINS', Id: 'TO' }
                ];

                return $q.when(estados);
            }

            function obterEstadosSuccess(dados) {
                if (dados.data)
                    dados = dados.data;
                $scope.formData.estados = dados;
                return $q.when();
            }

            $scope.regionaisChange = function () {
                $scope.distribuidor.DDD = null;
                $scope.distribuidor.IdMicroRegiao = null;
            }

            $scope.buscarFuncionario = function (text) {
                console.log('Buscando: ' + text);
                if (text) {
                    console.log('Query');
                    return WebAPI.getEntidadeApiComQueryString("busca-funcionario-por-nome",
                        "termo=" + text + "&idRegional=" + $scope.distribuidor.IdRegional)
                        .then(function (res) {
                            console.log('Res: ');
                            console.log(res);
                            return res.data.map(function (item) {
                                return {
                                    id: item.CodFuncionario,
                                    text: (item.NomeCompleto + ' - ' + item.Matricula + ' - ' + item.Cargo)
                                };
                            });
                        })
                        .catch(function (err) {
                            console.log(err);
                        });
                } else {
                    return $q.when([]);
                }
            };

            $scope.dddsChange = function () {
                // console.log("Mudança de ddd" + $scope.concorrencia.DDD);                
                $scope.distribuidor.IdMicroRegiao = null;
            }

            inicializarFormulario()
                .then(prepararDadosIniciais)
                .then(obterOperadoras)
                .then(obterOperadorasSuccess)
                .then(obterEstados)
                .then(obterEstadosSuccess)
                .then(obterSocios)
                .then(obterSociosSuccess)
                .then(obterIntegradoresDistribuidor)
                .then(obterIntegradoresDistribuidorSuccess)
                .then(obterIntegradores)
                .then(obterIntegradoresSuccess)
                .then(finalizarInicializacao)
                .catch(deuErro);

            $scope.validar = function () {
                // if (!$scope.distribuidor.NomeGNSellIn && !$scope.distribuidor.NomeGNSellOut)
                //     return $q.reject('Nome GN Sell In ou Sell Out deve ser informado');
                // if (($scope.distribuidor.NomeGNSellIn && !$scope.distribuidor.MatriculaGNSellIn)
                //     || (!$scope.distribuidor.NomeGNSellIn && $scope.distribuidor.MatriculaGNSellIn))
                //     return $q.reject('Dados de GN Sell In imcompletos');
                // if (($scope.distribuidor.NomeGNSellOut && !$scope.distribuidor.MatriculaGNSellOut) ||
                //     (!$scope.distribuidor.NomeGNSellOut && $scope.distribuidor.MatriculaGNSellOut))
                //     return $q.reject('Dados de GN Sell Out imcompletos');
                var distribuidor = $scope.distribuidor;
                distribuidor.CodigoExterno = distribuidor.CodigoExterno || UTIL.Guid();

                distribuidor.IdGNSellIn = ($scope.gnSellIn != null) ? $scope.gnSellIn.id : null;
                distribuidor.IdGNSellOut = ($scope.gnSellOut != null) ? $scope.gnSellOut.id : null;

                return $q.when({
                    list: [
                        {
                            Id: distribuidor.Id || $routeParams.id || null,
                            IdRegional: distribuidor.IdRegional,
                            DDD: distribuidor.DDD || null,
                            IdMicroRegiao: distribuidor.IdMicroRegiao,
                            Operadora: distribuidor.Operadora || null,
                            MarketShare: distribuidor.MarketShare || 0,
                            NomeGNSellIn: distribuidor.NomeGNSellIn || null,
                            MatriculaGNSellIn: distribuidor.MatriculaGNSellIn || null,
                            NomeGNSellOut: distribuidor.NomeGNSellOut || null,
                            /*10*/MatriculaGNSellOut: distribuidor.MatriculaGNSellOut || null,
                            RazaoSocial: distribuidor.RazaoSocial || null,
                            NomeFantasia: distribuidor.NomeFantasia || null,
                            NomeGrupo: distribuidor.NomeGrupo || null,
                            CNPJ: distribuidor.CNPJ || null,
                            CodigoSAP: distribuidor.CodigoSAP || null,
                            AMDOCS: distribuidor.AMDOCS || null,
                            InscricaoEstadual: distribuidor.InscricaoEstadual || null,
                            Endereco: distribuidor.Endereco || null,
                            ComplementoEndereco: distribuidor.ComplementoEndereco || null,
                            /*20*/EnderecoNumero: distribuidor.EnderecoNumero || null,
                            Bairro: distribuidor.Bairro || null,
                            Cidade: distribuidor.Cidade || null,
                            Estado: distribuidor.Estado || null,
                            PosicaoDaClaroNoDistribuidor: distribuidor.PosicaoDaClaroNoDistribuidor || null,
                            QuantidadeDeVendedores: distribuidor.QuantidadeDeVendedores || 0,
                            QuantidadeDePromotores: distribuidor.QuantidadeDePromotores || 0,
                            QuantidadeDeSupervisores: distribuidor.QuantidadeDeSupervisores || 0,
                            Situacao: distribuidor.Situacao || 'INATIVO',
                            DataDaEntrada: $filter("date")(distribuidor.DataDaEntrada, "yyyy-MM-dd hh:mm:ss") || null,
                            /*30*/DataDaSaida: $filter("date")(distribuidor.DataDaSaida, "yyyy-MM-dd hh:mm:ss") || null,
                            CodigoExterno: distribuidor.CodigoExterno,
                            MarketShareLider: distribuidor.MarketShareLider || 0,
                            CEP: distribuidor.CEP || null,
                            Populacao: distribuidor.Populacao || 0,
                            CodigoSAP2: distribuidor.CodigoSAP2 || null,
                            EmailGerComercial: distribuidor.EmailGerComercial || null,
                            EmailResFinanceiro: distribuidor.EmailResFinanceiro || null,
                            EmailResCompras: distribuidor.EmailResCompras || null,
                            AMDOCS2: distribuidor.AMDOCS2 || null,
                            /*40*/Gross: distribuidor.Gross || 0,
                            Insercao: distribuidor.Insercao || 0,
                            Faturamento: distribuidor.Faturamento || 0,
                            IdGNSellIn: distribuidor.IdGNSellIn || null,
                            IdGNSellOut: distribuidor.IdGNSellOut || null,
                            PercGNSellIn: distribuidor.PercGNSellIn || null,
                            PercGNSellOut: distribuidor.PercGNSellOut || null,
                            NomeMultiplicador: distribuidor.NomeMultiplicador || null,
                            EmailMultiplicador: distribuidor.EmailMultiplicador || null,
                            TelefoneMultiplicador : distribuidor.TelefoneMultiplicador || null
                        }
                    ],
                    listSocio: $scope.socios.map(function (socio) {
                        return {
                            id: socio.Id || null,
                            iddistribuidor: distribuidor.Id || $routeParams.id || null,
                            nome: socio.Nome || null,
                            cpf: socio.CPF || null,
                            celular: socio.Celular || null,
                            email: socio.Email || null,
                            CodigoExterno: distribuidor.CodigoExterno || null,
                            RG: socio.RG || null
                        };
                    }),
                    listIntegrador: $scope.integradores.map(function (integrador) {
                        return {
                            id: integrador.Id || null,
                            iddistribuidor: integrador.IdDistribuidor || $routeParams.id || null,
                            integrador: integrador.Integrador || null,
                            CodigoExterno: distribuidor.CodigoExterno
                        };
                    })
                });
            }

            $scope.salvar = function () {
                if (!$scope.loading) {
                    $scope.loading = true;

                    $scope.validar()
                        .then(DistribuidorService.salvar)
                        .then(function (dados) {
                            var data = dados.data;

                            console.log(data);

                            if (data.some(function (d) {
                                return d.erro == "true"
                            })) {
                                var msgs = data.map(function (d) {
                                    return d.msg;
                                });
                                return $q.reject(msgs.join(' '));
                            }

                            $scope.loading = false;
                            console.log(dados);
                            $scope.showMessage("Distribuidor salvo com sucesso");
                            $window.location.href = '#/distribuidores'
                        })
                        .catch(function (erro) {
                            $scope.loading = false;
                            erro = erro.data || erro;
                            erro = erro.msg || erro;
                            console.log(erro);
                            $scope.showError(erro.msg || erro);
                        });

                }
            };

            $scope.cancelarSocio = function () {
                $scope.socioSelecionado = null;
            };

            $scope.salvarSocio = function () {
                var socio = $scope.socioSelecionado;
                if (!socio.Id && $scope.socios.indexOf(socio) == -1)
                    $scope.socios.push(socio);
                $scope.socioSelecionado = null;
            };

            $scope.adicionarSocio = function () {
                if ($scope.socioSelecionado)
                    return;
                $scope.socioSelecionado = {};
            };

            $scope.editarSocio = function (socio) {
                $scope.socioSelecionado = socio;
            };

            $scope.excluirSocio = function (socio, index, $event) {
                var confirm = $mdDialog.confirm()
                    .title('Confirmar')
                    .textContent('Deseja realmente excluir o sócio?')
                    .ariaLabel('Confirmar')
                    .targetEvent($event)
                    .clickOutsideToClose(true)
                    .ok('Excluir')
                    .cancel('Cancelar');

                $mdDialog.show(confirm)
                    .then(function () {
                        $scope.socios.splice(index, 1);
                    }, function () {
                        console.log('Cancelou...');
                        //cancelou
                    });
            };

            $scope.podeSalvarSocio = function (socio) {
                return $scope.socioSelecionado && $scope.socioSelecionado.Nome
                    && $scope.socioSelecionado.Email && $scope.socioSelecionado.Celular
                    && $scope.socioSelecionado.CPF && $scope.socioSelecionado.RG;
            };

            $scope.cancelarIntegrador = function () {
                $scope.integradorSelecionado = null;
            };

            $scope.salvarIntegrador = function () {
                var integrador = $scope.integradorSelecionado;
                if (!integrador.Id && $scope.integradores.indexOf(integrador) == -1)
                    $scope.integradores.push(integrador);
                $scope.integradorSelecionado = null;
            };

            $scope.adicionarIntegrador = function () {
                if ($scope.integradorSelecionado)
                    return;
                $scope.integradorSelecionado = {};
            };

            $scope.editarIntegrador = function (integrador) {
                $scope.integradorSelecionado = integrador;
            };

            $scope.excluirIntegrador = function (integrador, index, $event) {
                var confirm = $mdDialog.confirm()
                    .title('Confirmar')
                    .textContent('Deseja realmente excluir o integrador?')
                    .ariaLabel('Confirmar')
                    .targetEvent($event)
                    .clickOutsideToClose(true)
                    .ok('Excluir')
                    .cancel('Cancelar');

                $mdDialog.show(confirm)
                    .then(function () {
                        $scope.integradores.splice(index, 1);
                    }, function () {
                        console.log('Cancelou...');
                        //cancelou
                    });
            };

            $scope.nomeIntegrador = function (id) {
                return $scope.formData.integradores.find(function (i) {
                    return i.Id == id;
                }).Nome;
            }

            $scope.integradoresNaoAdicionados = function (integradores) {

                return $scope.formData.integradores.filter(function (integrador) {
                    return !$scope.integradores.find(function (distIntegrador) {
                        return distIntegrador.Integrador == integrador.Id;
                    });
                });
            };

            $scope.podeSalvarIntegrador = function (socio) {
                return $scope.integradorSelecionado && $scope.integradorSelecionado.Integrador;
            };
        }])
    .filter('searchFor3', function () {
        return function (arr, searchString) {
            if (!searchString) {
                return arr;
            }

            var result = [];
            searchString = searchString.toLowerCase();
            angular.forEach(arr, function (item) {
                if (item.Regional.toLowerCase().indexOf(searchString) !== -1) {
                    result.push(item);
                }
            });
            return result;
        };
    });