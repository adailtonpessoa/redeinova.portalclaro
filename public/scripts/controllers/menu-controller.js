angular.module("orbe.controllers")
    .controller('MenuController', function ($scope, $rootScope, $timeout, $mdSidenav, USER, $window, $mdDialog, CONFIG, $interval) {
        USER.restore();
        $scope.$root.pages = [];
        var menuController = angular.element(document.querySelector("#main_menu")).scope();

        if (USER.menu) {
            menuController.sections = USER.menu;
            for (var i = 0; i < menuController.sections.length; i++) {
                if (menuController.sections[i].url) {
                    $scope.$root.pages.push(menuController.sections[i]);
                }
                $scope.$root.pages = $scope.$root.pages.concat(menuController.sections[i].pages);
            }

            if ($scope.$root.pages.length) {
                $window.location.href = '#' + $scope.$root.pages[0].url;
                // console.log($window.location.href);
            }
        }

        $scope.sections = USER.menu;

        menuController.$watch('sections', function (s) {
            $scope.$root.pages = [];
            $scope.sections = s;
            $scope.isLoggedIn = typeof (s) != 'undefined';
            if (s)
                for (var i = 0; i < s.length; i++) {
                    if (s[i].url) {
                        $scope.$root.pages.push(s[i]);
                    }
                    $scope.$root.pages = $scope.$root.pages.concat(s[i].pages);
                }
            var viewIndex = -1;
            // console.log($scope.$root.pages);
            if ($scope.$root.pages.length) {
                $window.location.href = '#' + $scope.$root.pages[0].url;
                // console.log($window.location.href);
            }
        });
        var self = this;

        $scope.path = path;
        $scope.goHome = goHome;
        $scope.openMenu = openMenu;
        $scope.closeMenu = closeMenu;
        $scope.isSectionSelected = isSectionSelected;
        $scope.isFullScreen = false;

        $rootScope.$on('$locationChangeSuccess', openPage);

        $scope.focusMainContent = focusMainContent;

        this.isOpen = isOpen;
        this.isSelected = isSelected;
        this.toggleOpen = toggleOpen;
        this.autoFocusContent = false;

        var mainContentArea = document.querySelector("[role='main']");

        var menu = {
            selectSection: function (section) {
                this.openedSection = section;
            },
            toggleSelectSection: function (section) {
                this.openedSection = (self.openedSection === section ? null : section);
            },
            isSectionSelected: function (section) {
                return this.openedSection === section;
            },

            selectPage: function (section, page) {
                this.currentSection = section;
                this.currentPage = page;
            },
            isPageSelected: function (page) {
                return this.currentPage === page;
            },
            sections: $scope.sections
        };

        function closeMenu() {
            $timeout(function () { $mdSidenav('left').close(); });
        }

        function openMenu() {
            $timeout(function () { $mdSidenav('left').open(); });
        }

        function path() {
            return $location.path();
        }

        function goHome($event) {
            menu.selectPage(null, null);
            $location.path('/');
        }

        function openPage() {
            $scope.closeMenu();

            if (self.autoFocusContent) {
                focusMainContent();
                self.autoFocusContent = false;
            }
        }

        $scope.isItemSelected = function (section) {
            return $scope.$root.currentPage == section.name;
        };

        function focusMainContent($event) {
            if ($event) { $event.preventDefault(); }

            $timeout(function () {
                mainContentArea.focus();
            }, 90);

        }

        function isSelected(page) {
            return menu.isPageSelected(page);
        }

        function isSectionSelected(section) {
            var selected = false;
            var openedSection = menu.openedSection;
            if (openedSection === section) {
                selected = true;
            }
            else if (section.children) {
                section.children.forEach(function (childSection) {
                    if (childSection === openedSection) {
                        selected = true;
                    }
                });
            }
            return selected;
        }

        var menuSections = {

        };

        for (var i in $scope.sections) {
            menuSections[$scope.sections[i]] = { isOpen: false };
        }


        function isOpen(section) {


            return menuSections[section].isOpen;

        }

        function toggleOpen(section) {
            menuSections[section].isOpen = !menuSections[section].isOpen;

        }

        $scope.$root.forceLogout = function () {
            USER.clear();
            var fullUrl = $window.location.href.toLowerCase();
            fullUrl = fullUrl.substring(0, fullUrl.indexOf('#'));
            // console.log('URL: ' + fullUrl);
            $window.location = fullUrl + '/#/';
        };
        $scope.$root.checkLogin = function () {
            USER.restore();
            // console.log(USER)
            if (!USER.signature) {
                var fullURL = $window.location.href;
                if (fullURL.indexOf('/#/login') == -1) {
                    $window.location.href = "#/login";
                }
            }
        };

        $scope.isSectionSelected = function (section) {
            return $rootScope.currentPage == section.name;
        };

        $scope.sair = function () {

            var fullUrl = $window.location.href.toLowerCase();
            fullUrl = fullUrl.substring(0, fullUrl.indexOf('/#'));
            $window.location.href = fullUrl + '/#/logout/';
            // console.log($window.location.href);
        };

        USER.restore();
        
        function definirNomeUsuario() {
            $scope.$root.nomeUsuario = (USER.name || ' ').split(' ')[0];
        }

        $scope.$on("logou", definirNomeUsuario);

    });