angular.module("orbe.controllers")
    .controller('MainController', function ($scope, $mdSidenav, $mdDialog, $interval, $window, $mdToast) {

        $scope.opts = {
            isFullScreen: false,
            duration: 60,
            transition: false
        };
        var viewIndex = -1;

        var panelRef = null;

        $scope.openFullDialog = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $mdSidenav('right').toggle();
        };

        $scope.fecharDialogo = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.isFullScreen = false;
            $mdDialog.hide(panelRef);
        };


        $scope.confirmarDialogo = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $mdSidenav('right').toggle();

            $scope.isFullScreen = true;
            function mudarDash() {
                viewIndex++;
                if (viewIndex > $scope.$root.pages.length - 1)
                    viewIndex = 0;
                var url = $scope.$root.pages[viewIndex].url;
                if (url.indexOf('dashboard/view') == -1)
                    url = '/dashboard/view/' + url;
                $window.location.href = '#' + $scope.$root.pages[viewIndex].url;
            }
            if ($scope.opts.transition) {
                mudarDash();
                $scope._it = $interval(mudarDash, $scope.opts.duration * 1000);
            }
            console.log('Entering fullscreen mode...');
            $scope.leaveFullscreen = function ($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.isFullScreen = false;
                if ($scope._it) {
                    $interval.cancel($scope._it);
                }

                // $scope.$root.$apply();
            };
        };

        $scope.collapseMenu = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.isMenuCollapsed = true;

        };
        $scope.expandMenu = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.isMenuCollapsed = false;

        };

        $scope.sectionClicked = function ($event, section) {
            if (section.type == 'toggle') {
                section.menu($event);
            }
            else {
                console.log('Link');
                console.log(section);
                $window.location.href = '#' + section.url;
            }
        };

        $scope.selectPage = function ($event, page) {
            $window.location.href = '#' + page.url;
        };

        $scope.mouseEnter = function ($event, section) {
            section.mouseEnter = true;
            console.log(section);
            if (section.pages.length) {
                section.menu($event);
            }

        };

        $scope.mouseLeave = function ($event, section) {
            section.mouseEnter = false;
            console.log(section);
        };

        $scope.showMessage = function(msg) {
            $mdToast.show(
                $mdToast.simple()
                // .toastClass("normal-toast")
                .textContent( msg )
                .position('top right')
                .hideDelay(5000)
            );
        };

        $scope.showError = function(msg) {
            console.log('An Error Happened ');
            console.log(msg);
            $mdToast.show(
                $mdToast.simple()
                .textContent(msg)
                .position('top right')                
                .hideDelay(5000)
            );
        };

    }).config(function ($mdDateLocaleProvider) {
        $mdDateLocaleProvider.formatDate = function (date) {
            return date ? moment(date).format('DD-MM-YYYY') : '';
        };

        $mdDateLocaleProvider.parseDate = function (dateString) {
            var m = moment(dateString, 'DD-MM-YYYY', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
    });