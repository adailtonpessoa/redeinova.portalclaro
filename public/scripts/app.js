"use strict";

angular.module('orbe', ['ngMaterial','ngRoute','ngAnimate', 'ngAria', 'ngCookies', 'ui.router', 'orbe.controllers', 'orbe.directives', 'orbe.filters', 'orbe.services', 'angular-jwt', 'ui.utils.masks'])
.constant("CONFIG", {
    "API_URL": "",
    AUTH_URL: 'http://192.168.0.90:3031/autenticar',
    RESOLUTO_SERVER: 'https://resolutoserver.redeinova.com.br/trusted'
})
.config(['$routeProvider','$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$mdThemingProvider', '$sceDelegateProvider', '$mdDateLocaleProvider',
function ($routeProvider,$stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $mdThemingProvider,$sceDelegateProvider, $mdDateLocaleProvider) {
     
     $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'https://resolutoserver.redeinova.com.br/**'
    ]);
     
    $routeProvider
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'HomeController'
        })
        .when('/login', {
            name : 'orbe.login',
            templateUrl: 'pages/login.html',
            controller: 'LoginController'
        })
        .when('/logout', {
            name : 'orbe.logout',
            templateUrl: 'pages/logout.html',
            controller: 'LogoutController'
        })
        .when('/dashboard/view/:view*', { 
            name : 'orbe.dashboard',
            templateUrl: 'pages/dashboard.html',
            controller: 'DashController' 
        })  
        .when('/funcionarios', { 
            templateUrl: 'pages/funcionarios.html',
            controller: 'FuncionariosController' 
        })
        .when('/funcionarios/new', { 
            templateUrl: 'pages/funcionario.html',
            controller: 'FuncionarioController' 
        })
        .when('/funcionarios/:id/edit', { 
            templateUrl: 'pages/funcionario.html',
            controller: 'FuncionarioController' 
        })
        .when('/concorrencias/new', { 
            templateUrl: 'pages/concorrencia.html',
            controller: 'ConcorrenciaController' 
        })
        .when('/concorrencias', { 
            templateUrl: 'pages/concorrencias.html',
            controller: 'ConcorrenciasController' 
        })
        .when('/concorrencias/:id/edit', { 
            templateUrl: 'pages/concorrencia.html',
            controller: 'ConcorrenciaController' 
        })
        .when('/distribuidores', { 
            templateUrl: 'pages/distribuidores.html',
            controller: 'DistribuidoresController' 
        })
        .when('/distribuidores/new', { 
            templateUrl: 'pages/distribuidor.html',
            controller: 'DistribuidorController' 
        })
        .when('/distribuidor/:id/edit', { 
            templateUrl: 'pages/distribuidor.html',
            controller: 'DistribuidorController' 
        })
        .otherwise({
            redirectTo: '/'
        });

        $mdThemingProvider.theme('default')
            .primaryPalette('orange', {
            'default': '600', // by default use shade 400 from the pink palette for primary intentions
            'hue-1': '200', // use shade 100 for the <code>md-hue-1</code> class
            'hue-2': '300', // use shade 600 for the <code>md-hue-2</code> class
            'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
            });
        
        //$urlRouterProvider.otherwise ( 'home' );
}])
.run(['$templateCache', '$rootScope', '$window', 'USER',
    function ($templateCache, $rootScope, $window, USER) {
        
        USER.restore();
        if(!USER.signature)  {
            var fullURL = $window.location.href;
            if(fullURL.indexOf('/#/login') == -1){
                $window.location.href = "#/login";
            }
        }
      
        $rootScope.isLoggedIn = USER.signature != null && USER.signature != '';
}]);